package com.topperji.m2m;



public class DefineHeaderFrame {
	
public final int HEADER_SIZE = 36;
	
	public final byte[] FS = { (byte)0xCA, (byte)0x35 };
	public final byte[] FE = { (byte)0xAC, (byte)0x53 };
	
	public final byte[] ACK = { (byte) 0xCA, (byte) 0x00,(byte) 0x01,(byte) 0x35 };
	public final byte[] NAK_CRC = { (byte) 0xCA, (byte) 0x00,(byte) 0x11,(byte) 0x35 };
	public final byte[] NAK_NO = { (byte) 0xCA, (byte) 0x00,(byte) 0x10,(byte) 0x35 };
	public final byte[] NAK_FS = { (byte) 0xCA, (byte) 0x00,(byte) 0x12,(byte) 0x35 };
	public final byte[] NAK_FE = { (byte) 0xCA, (byte) 0x00,(byte) 0x13,(byte) 0x35 };
	
	public final byte[] XML_START = { (byte) 0x4c, (byte) 0x4f, (byte) 0x4e, (byte) 0x45 };
	public final byte[] XML_END = { (byte) 0x23 };
	public final byte[] XML_ACK = { (byte) 0x4c, (byte) 0x4f, (byte) 0x4e, (byte) 0x45, (byte) 0x00, (byte) 0x23  };
	public final byte[] XML_NAK = { (byte) 0x4c, (byte) 0x4f, (byte) 0x4e, (byte) 0x45, (byte) 0x01, (byte) 0x23  };
	
	
	private int type;
	private String date;
	private String time;
	private int serverNum;
	private int hid;
	private String userInfo_type;
	private String userInfo;
	private int nextFrame_left;
	private int nextFrame_size;
	
	public void analysisProtocol(byte[] byteInput) {
		
		type =  2;
//		date = String.valueOf( (int)byteInput[2] ) +StringUtils.leftPad( String.valueOf( (int)byteInput[3] ), 2,"0") 
//				+ StringUtils.leftPad(String.valueOf( (int)byteInput[4] ),2,"0") 
//				+ StringUtils.leftPad(String.valueOf( (int)byteInput[5] ),2,"0");
//		time = StringUtils.leftPad( String.valueOf( (int)byteInput[6] ), 2,"0") 
//				+  StringUtils.leftPad(String.valueOf( (int)byteInput[7] ),2,"0") 
//				+ StringUtils.leftPad(String.valueOf( (int)byteInput[8] ),2,"0");
		date = String.valueOf( (int)byteInput[2] ) + String.format("%02d", (int)byteInput[3] ) 
				+ String.format("%02d", (int)byteInput[4] ) + String.format("%02d", (int)byteInput[5] );
		time = String.format("%02d", (int)byteInput[6] ) 
				+ String.format("%02d", (int)byteInput[7] ) + String.format("%02d", (int)byteInput[8] );
		serverNum = (int)byteInput[9];
		
		// byte -> int
		hid = 0;
		hid |= ((byteInput[10]) << 24) & 0xFF000000;
		hid |= ((byteInput[11]) << 16) & 0xFF0000;
		hid |= ((byteInput[12]) << 8) & 0xFF00;
		hid |= ((byteInput[13]) ) & 0xFF;
		//System.out.println( StringUtils.leftPad(Integer.toHexString(hid), 8,"0") );
		
		userInfo_type = String.valueOf( (char)byteInput[14]);
		
//		if( userInfo_type == "P") {
//			userInfo_num = 0;
//		} else if( userInfo_type == "I") {
//			userInfo_num = 1;
//		} else if( userInfo_type== "C") {
//			userInfo_num = 2;
//		} else {
//			userInfo_num = -1;
//		}
		
		userInfo = "";
		for(int i=0; i < 13; i++ ) {
			userInfo += (char)byteInput[15+i];
		}
		userInfo = userInfo.trim();
		
		nextFrame_left = 0;
		nextFrame_left |= ((byteInput[28]) << 8) & 0xFF00;
		nextFrame_left |= ((byteInput[29]) ) & 0xFF;
		
		nextFrame_size = 0;
		nextFrame_size |= ((byteInput[30]) << 8) & 0xFF00;
		nextFrame_size |= ((byteInput[31]) ) & 0xFF;
		
	}	
	
	public void setHeaderInfo(String date, String time, int hid, String userInfoType, String gwNo, int nextFrameLeft, int nextFrameSize, int serverNum ){
		
		this.date = date;
		this.time = time;
		this.hid = hid;
		this.userInfo_type = userInfoType;
		this.userInfo = gwNo;
		this.nextFrame_left = nextFrameLeft;
		this.nextFrame_size = nextFrameSize;
		this.serverNum = serverNum;
		
	}
	

	//Header frame maker
	public byte[] makeHeaderFrame( ) {
		byte[] resultFrame = new byte[36];
		
		resultFrame[0] = (byte) FS[0];
		resultFrame[1] = (byte) FS[1];
		resultFrame[34] = (byte) FE[0];
		resultFrame[35] = (byte) FE[1];
		//date
		resultFrame[2] = (byte) Integer.parseInt( date.substring(0, 2) );
		resultFrame[3] = (byte) Integer.parseInt( date.substring(2, 4) );
		resultFrame[4] = (byte) Integer.parseInt(  date.substring(4, 6) );
		resultFrame[5] = (byte) Integer.parseInt(  date.substring(6, 8) );
		//time
		resultFrame[6] = (byte) Integer.parseInt( time.substring( 0, 2) );
		resultFrame[7] = (byte) Integer.parseInt( time.substring( 2, 4) );
		resultFrame[8] = (byte) Integer.parseInt( time.substring( 4, 6) );
		//차수
		resultFrame[9] = (byte)serverNum;
		//hid
		resultFrame[10] = (byte) ((hid & 0xff000000 ) >> 24); 
		resultFrame[11] = (byte) ((hid & 0x00ff0000 ) >> 16);
		resultFrame[12] = (byte) ((hid & 0x0000ff00 ) >> 8);
		resultFrame[13] = (byte) ((hid & 0x000000ff ) );
		//userInfo type
		resultFrame[14] = (byte)userInfo_type.charAt(0);
		//userInfo
		for( int i=0; i < 13; i++ ) {
			if( i < userInfo.length() ) {
				resultFrame[15+i] = (byte) userInfo.charAt(i);
			} else {
				resultFrame[15+i] = (byte)0x00;
			}
		}
		//nextFrame_left
		resultFrame[28] = (byte) ((nextFrame_left & 0xff00) >> 8);
		resultFrame[29] = (byte) ((nextFrame_left & 0x00ff) );
		//nextFrmae_size
		resultFrame[30] = (byte) ((nextFrame_size & 0xff00) >> 8);
		resultFrame[31] = (byte) ((nextFrame_size & 0x00ff));
		//CRC
		short tempCrc = CRC16.updateH_crc((short)0, resultFrame, (short)(resultFrame.length-4) );
		resultFrame[32] = (byte) ((tempCrc & 0xff00) >> 8);
		resultFrame[33] = (byte) ((tempCrc & 0x00ff) );

		//System.out.printf( "###!!!!!!! : %02x", CRC16.updateH_crc((short)0, resultFrame, (short)(resultFrame.length-2) ) );
		return resultFrame;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @param serverNum the serverNum to set
	 */
	public void setServerNum(int serverNum) {
		this.serverNum = serverNum;
	}

	/**
	 * @param hid the hid to set
	 */
	public void setHid(int hid) {
		this.hid = hid;
	}

	/**
	 * @param userInfo_type the userInfo_type to set
	 */
	public void setUserInfo_type(String userInfo_type) {
		this.userInfo_type = userInfo_type;
	}


	/**
	 * @param userInfo the userInfo to set
	 */
	public void setUserInfo(String userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * @param nextFrame_left the nextFrame_left to set
	 */
	public void setNextFrame_left(int nextFrame_left) {
		this.nextFrame_left = nextFrame_left;
	}

	/**
	 * @param nextFrame_size the nextFrame_size to set
	 */
	public void setNextFrame_size(int nextFrame_size) {
		this.nextFrame_size = nextFrame_size;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @return the serverNum
	 */
	public int getServerNum() {
		return serverNum;
	}

	/**
	 * @return the hid
	 */
	public int getHid() {
		return hid;
	}

	/**
	 * @return the userInfo_type
	 */
	public String getUserInfo_type() {
		return userInfo_type;
	}

	/**
	 * @return the userInfo
	 */
	public String getUserInfo() {
		return userInfo;
	}
	/**
	 * @return the nextFrame_left
	 */
	public int getNextFrame_left() {
		return nextFrame_left;
	}

	/**
	 * @return the nextFrame_size
	 */
	public int getNextFrame_size() {
		return nextFrame_size;
	}

}
