package com.topperji.m2m;

import android.util.Log;


/**
 * 게이트웨이와 통신할 데이타 프레임을 생성하고 체크 하는 클래스
 * @author BMK-GHST
 *
 */
public class CreateDTMFFrame {
	static final char FS = '*';
	
	static final char FE ='#';
	
	public static final char SYSTEM_ACK = '0';
	public static final char SYSTEM_NAK = '1';
	public static final char SYSTEM_START = '2';
	public static final char SYSTEM_DATA_END = '3';
	public static final char SYSTEM_LINK_END = '4';
	
	/**
	 * System 메시지를 생성하는 함수
	 * @param systemMessage System 메시지, 0~3 
	 * @return 생성된 DTMF String
	 */
	public static String CreateDTMFLink (char systemMessage ){
		StringBuffer strBuf = new StringBuffer();
		strBuf.append(FS);
		strBuf.append(systemMessage);
		strBuf.append(FE);
		return strBuf.toString();
	}
	
	/**
	 * 전송 받은 DTMF String 을 체크 하는 함수
	 * @param message 전송받은 DTMF
	 * @return 처리 결과, 정상이면 HID + Data 리턴
	 */
	public static String CheckDTMFFrame( String message ){
		int count = 0;
		String strFS = message.substring(count , ++count);		
		String strLeng = message.substring(count , count+=2 );
		short length = (short)Integer.parseInt(strLeng);
		String strHID = message.substring(count, count+=2);
		String strData = message.substring(count, count+=(length-2));		
		String strCRC = message.substring(count , count+=3);
		byte crc = (byte)Integer.parseInt(strCRC);
		String strFE = message.substring(count , ++count);
		//check frame;
		
		if( strFS.getBytes()[0] != CreateDTMFFrame.FS){
			// FS 에러 
			Log.i("Lia","FS ERROR");
			return null;
		}
		if( strFE.getBytes()[0] != CreateDTMFFrame.FE){
			// FE 에러
			Log.i("Lia","FE ERROR");
			return null;
		}
		
		byte[] checkFrame = new byte[message.length()-4];
		String strCheck = message.substring(1,message.length()-4 );
		byte[] temp = strCheck.getBytes();
		//System.out.println(strCheck);
		for( int i = 0 ; i < temp.length ; i++ ){
			checkFrame[i] =(byte)(Integer.parseInt(String.format("%c",temp[i]))); 
		} 
		checkFrame[checkFrame.length-1] = crc;  
		if( CRC8.crc_8(checkFrame) != 0){
			// CRC ERROR
			Log.i("Lia","CRC ERROR");
			return null;
		}
		                             
		
		return strHID+strData;
	}
	
	/**
	 * DTMF String을 생성하는 함수
	 * @param HID 전송 HID
	 * @param data 전송할 Data
	 * @return 처리 결과
	 */
	public static String CreateDTMF(short HID , byte[] data){ 
		short Length;
		byte CRC;
		byte[] temp; 
		byte[] tempFrame;
		int countFrame = 0;
		int count = 0;
		if( data == null ){
			Length = 2;
			temp = new byte[Length+2];
			tempFrame = new byte[temp.length + 3];
			tempFrame[countFrame++] = temp[count++] = (byte)((Length%100)/10); 
			tempFrame[countFrame++] = temp[count++] = (byte)(Length%10);
			tempFrame[countFrame++] = temp[count++] = (byte)((HID%100)/10);
			tempFrame[countFrame++] = temp[count++] = (byte)(HID%10);
		}else { 
			Length = (short) (2 + data.length);
			temp = new byte[Length+2];
			tempFrame = new byte[temp.length + 3];
			tempFrame[countFrame++] = temp[count++] = (byte)((Length%100)/10);
			tempFrame[countFrame++] = temp[count++] = (byte)(Length%10);
			tempFrame[countFrame++] = temp[count++] = (byte)((HID%100)/10);
			tempFrame[countFrame++] = temp[count++] = (byte)(HID%10);
			for( int i = 0 ; i < data.length; i++){
				tempFrame[countFrame++] = temp[count++] = data[i];
			} 
		}
		
		CRC = CRC8.crc_8(temp);
		for( int i = 0 ; i < temp.length ; i++ ){
			System.out.printf("%02X ", temp[i]);
		}
		System.out.println();
		int nTemp = Integer.parseInt(String.format("%x",CRC), 16);
		tempFrame[countFrame++] = (byte)(nTemp/100);
		tempFrame[countFrame++] = (byte)((nTemp%100)/10);
		tempFrame[countFrame++] = (byte)(nTemp%10);
 
		StringBuffer returnVal = new StringBuffer();
		returnVal.append(FS);
		for( int i = 0 ; i < tempFrame.length ; i++ )
			returnVal.append(Integer.parseInt(String.format("%x",tempFrame[i]), 16));  
		returnVal.append(FE);  
		return new String( returnVal );
	}
}
