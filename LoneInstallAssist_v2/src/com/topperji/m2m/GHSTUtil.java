package com.topperji.m2m;


import java.util.Calendar;

/**
 * ������Ʈ�� UTIL�� ��� �ϴ� Ŭ����
 * @author BMK-GHST
 *
 */
public class GHSTUtil { 
	
	/**
	 * ���� Ÿ�� �ƿ� , ���� �и�
	 * @return ���� Ÿ�� �ƿ� �и� ������ ��
	 */
	public synchronized static int SocketTimeOutMillis(){
		return 3000;
	}
	/**
	 * ���� Ÿ�� �ƿ� , ���� �и�
	 * @return ���� Ÿ�� �ƿ� �и� ������ ��
	 */
	public synchronized static int SocketDTMFDAP1_TimeOutMillis(){
		return 3000;
	}
	/**
	 * ���� �ð� ���� Byte ���۷� ��ȯ �ϴ��Լ�
	 * @return Byte array
	 */
	public synchronized byte[] CalendarFrameByte(){
		Calendar car = Calendar.getInstance();
		byte time[] = {
					(byte)(car.get(Calendar.YEAR)/100),
					(byte)(car.get(Calendar.YEAR)%100),
					(byte)(car.get(Calendar.MONTH)+1),
					(byte)(car.get(Calendar.DAY_OF_MONTH)),
					(byte)(car.get(Calendar.HOUR_OF_DAY)),
					(byte)(car.get(Calendar.MINUTE)),
					(byte)(car.get(Calendar.SECOND))
				};
		return time; 
	}
	
	/**
	 * Byte Array �� String �� String ���� ��ȯ ���ִ� �Լ�
	 * @param buf String ����
	 * @return ���� �� String
	 */
	public synchronized String GetString ( byte[] buf ){
		StringBuffer strbuf = new StringBuffer();
		for( int i = 0 ; i < buf.length ; i++ ){
			if( buf[i] == 0x00){
				continue; 
			}
			strbuf.append((char)buf[i]);
		}
		return new String(strbuf);
	}
	
	/**
	 * ���� �ð��� String ���� ��ȯ���ִ� �Լ� ( yyyy_mm_dd )
	 * @return ���� �� String
	 */
	public synchronized String CalendarFrameStrForDay(){
		Calendar car = Calendar.getInstance();
		String time;
		time = String.format("%04d_%02d_%02d", (int)car.get(Calendar.YEAR) , 
				(int)car.get(Calendar.MONTH)+1,
				(int)car.get(Calendar.DAY_OF_MONTH)); 
		return time; 
	}
	
	
	/**
	 * ���� �ð��� String ���� ��ȯ���ִ� �Լ� ( yyyy-mm-dd HH:MM:SS )
	 * @return ���� �� String
	 */
	public synchronized String CalendarFrameStr(){
		Calendar car = Calendar.getInstance(); 
		return  String.format("%04d-%02d-%02d %02d:%02d:%02d", (int)car.get(Calendar.YEAR) , 
				(int)car.get(Calendar.MONTH)+1,
				(int)car.get(Calendar.DAY_OF_MONTH),
				(int)car.get(Calendar.HOUR_OF_DAY),
				(int)car.get(Calendar.MINUTE),
				(int)car.get(Calendar.SECOND));  
	}

	/**
	 * ���� �ð��� String ���� ��ȯ���ִ� �Լ� ( yyyy-mm-dd )
	 * @return ���� �� String
	 */
	public synchronized String CalendarDateFrameStr(){
		Calendar car = Calendar.getInstance(); 
		return  String.format("%04d-%02d-%02d", (int)car.get(Calendar.YEAR) , 
				(int)car.get(Calendar.MONTH)+1,
				(int)car.get(Calendar.DAY_OF_MONTH));  
	}
	public synchronized String CalendarDateFrameStr(int monthadd){
		Calendar car = Calendar.getInstance(); 
		return  String.format("%04d-%02d-%02d", (int)car.get(Calendar.YEAR) , 
				(int)car.get(Calendar.MONTH)+1,
				(int)car.get(Calendar.DAY_OF_MONTH)+monthadd);  
	}
	/**
	 * Byte ������ ���� 16��� String ���� ��ȯ �ϴ� �Լ�
	 * @param buf Data ����
	 * @return ���� �� String
	 */
	public synchronized String GetStringFromByte ( byte[] buf ){
		StringBuffer strbuf = new StringBuffer();
		for( int i = 0 ; i < buf.length ; i++ ){
			strbuf.append(String.format("%02X", buf[i]));
		}
		return strbuf.toString();
	}
	
	
	/**
	 * String�� 16��� ���� Byte ���۷� ��ȯ �ϴ� �Լ�
	 * @param strBuf 16��� String
	 * @return ���� �� String
	 */
	public synchronized byte[] GetByteFromString( String strBuf ){
		byte[] buf = null;
		int strBufCount = 0;
		strBuf = strBuf.replace(" ", "");
		if( strBuf.length() % 2 != 0){ 
			return null;
		}
		buf = new byte[ strBuf.length() / 2 ];
		for( int i = 0 ; i< buf.length ; i ++ ){
			try {
				buf[i] = (byte)Integer.parseInt(strBuf.substring(strBufCount , strBufCount += 2),16); 
			}catch(Exception e){
				e.printStackTrace();
				return null;
			}
		} 
		return buf;
	}
	
	/**
	 * ��� ���� Data�� �����ϱ� �� ���� Transaction ID�� ����� �Լ�
	 * @param userNumber ���� �� ��ȭ ��ȣ
	 * @return �� �� String
	 */
	public synchronized String CreateTID( String userNumber ){
		StringBuffer buf = new StringBuffer();
		Calendar car = Calendar.getInstance();
		buf.append(userNumber);
		buf.append(String.format("%02d",car.get(Calendar.DAY_OF_MONTH)));
		buf.append(String.format("%02d",car.get(Calendar.HOUR_OF_DAY)));
		buf.append(String.format("%02d",car.get(Calendar.MINUTE)));
		buf.append(String.format("%02d",car.get(Calendar.SECOND)));
		return new String(buf);
	}
	
	
}
