package com.topperji.m2m;

import java.util.LinkedList;

/**
 * �̻��
 * @author BMK-GHST
 *
 */
public class DTMFNumberList {
	private static DTMFNumberList object = null;
	public static DTMFNumberList getInstance(){
		if( object == null )
			object = new DTMFNumberList();
		return object;
	}
	
	private LinkedList<String> numberList = null; 
	private DTMFNumberList(){
		numberList = new LinkedList<String>();
	}	
	public synchronized String poll(){
		if( numberList == null ){
			return null;
		}
		try{
			synchronized ( numberList ){
				if( numberList.size() == 0) 
					this.wait();
			}
		}catch( Exception e){
			e.printStackTrace();
		}
		return numberList.poll();
	}
	public synchronized void add( String data){
		if( numberList == null ){
			return;
		}
		synchronized ( numberList ){
			numberList.add(data); 
			this.notify();
		}
	}
}
