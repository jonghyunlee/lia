package com.topperji.httpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

public class HttpMultiPartPost {
	
	//valid 키를 기본적으로 탑재. 자동으로 로드 되도록. 마찬가지로 암호화 

	StringBuffer sbResponse = new StringBuffer("");
	
	String url;
//	String url = "http://222.122.128.21/m2m_manager/callInterface/liaCall.php?"; //url 변수 ->XML로 뺀다.
	
	ArrayList<BasicNameValuePair> requestParameter;
	
	public HttpMultiPartPost() {
		requestParameter = new ArrayList<BasicNameValuePair>();
		url = new String();
	}
	
	public HttpMultiPartPost( String url ) {
		requestParameter = new ArrayList<BasicNameValuePair>();
		this.url = url;
	}
	
	/**
	 * 접근 키정보 셋팅
	 */
	public void setKey( String param) {
		addParameter("voidjsKeycndlaCodea12377d8a9DFvndlsc", param);
	}
	
	/**
	 * 접속할 서버의 완전한 URL 형태. http:// ~ 파일명까지 명시.
	 * @param url
	 * @see HttpMultiPartPost#getServerUrl()
	 */
	public void setServerUrl( String url ) {
		this.url = url;
	}
	
	/**
	 * 현재 셋팅된 서버 URL 값을 반환한다.
	 * @return url 값
	 * @see #setServerUrl(String)
	 */
	public String getServerUrl() {
		return this.url;
	}
	
	/**
	 * 처리될 페이지에서 필요한 post 방식의 파라메터 입력. 말하자면 ?parma=value 의 형태이다.
	 * 객체가 생성되고 나면 파라메터는 계속 누적되므로, 사용후 리셋해야한다.
	 * @param paramName 파라메터 이름
	 * @param paramValue 파라메터 값
	 * @see HttpMultiPartPost#resetParameter()
	 */
	public void addParameter( String paramName, String paramValue ) {
		requestParameter.add( new BasicNameValuePair( paramName, paramValue ) );
		
	}
	
	/**
	 * 파라메터 배열을 초기화 한다. 
	 * @see HttpMultiPartPost#addParameter(String, String)  
	 */
	public void resetParameter() {
		requestParameter.clear();
		
	}
	
	public int executeMultipartsPost() throws ClientProtocolException, IOException {	//리턴을 수정
		
		int returnMessage = 0;	//리턴 메세지 
		
		BufferedReader in = null;
		/*post parameter 생성*/
		
			
		HttpPost request = new HttpPost(url);
		request.setEntity( new UrlEncodedFormEntity( requestParameter, HTTP.UTF_8 ) ); // execute를 위한 request 생성 완료	- 보내는 인코딩을 UTF-8로 설정
		
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = client.execute(request);  //페이지 요청에 대한 응답 객체 생성
		
		InputStreamReader objectResponse = new InputStreamReader(response.getEntity().getContent()); //response 응답 stream 저장
		
		in = new  BufferedReader(objectResponse);
		
		String line = "";
		String NL = System.getProperty("line.separator");
		
		while((line = in.readLine()) != null) {
			sbResponse.append(line + NL);
		}
		
//		returnMessage = FrsHttpMessageType.HTTP_RESPONSE_MESSAGE_SUCCESS;
			
		return returnMessage;
	}
	
	/**
	 * executeMultipartsPost() 수행 후 응답된 데이터를 가져옴.
	 * @return
	 * @see #executeMultipartsPost()
	 */
	public String getResponse() {
		return this.sbResponse.toString();
	}
	
	/**
	 * 응답된 메세지 버퍼를 초기화 함.
	 */
	public void resetResponse() {
		this.sbResponse.delete(0, sbResponse.length() );
	}
	
	/**
	 * 파라메터, 응답 버퍼, url을 한꺼번에 리셋.
	 */
	public void reset() {
		resetResponse();
		resetParameter();
		url = "";
	}
}


/*try {  //걍 페이지 정보 받아오기

URL text = new URL ( "http://192.168.0.104/EMS_server/pages/test.php");

InputStream isText = text.openStream();
byte[] bText = new byte[1050];
int readSize = isText.read(bText);
Log.i("Net", "readSize = " + readSize);
Log.i("Net", "bText = " + new String(bText));
isText.close();

} catch(Exception e) {
Log.e("Net", "Error in network call", e);
}
*/