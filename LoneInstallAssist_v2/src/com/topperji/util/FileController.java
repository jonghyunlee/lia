package com.topperji.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import android.content.Context;
import android.util.Log;


/**
 * 안드로이드 파일 컨트롤러 - 앱폴더 아래에 관련 파일을 생성하고 사용함.
 * @author Topper
 *
 */
public class FileController {
	
	File outputFile;
	
	boolean isFileExits;
	Context context;
	
	public FileController(Context context, File appPath, String fileName  ) {
		
		this.context = context;
		
//		File file = context.getDir("appDir", Activity.MODE_PRIVATE);
//		File file = Environment.getExternalStorageDirectory().getAbsoluteFile();
//		String appPath = file.getAbsolutePath();
//		String appPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		
		Log.i("Lia", appPath.getAbsolutePath() );
		
		/*폴더가 없으면 생성시킴*/
		if( !appPath.exists() ) {
			appPath.mkdirs();
		}
		
		outputFile = new File(appPath + "/" + fileName );

		/* 파일이 없으면 생성시킴. */
		if( !outputFile.exists() ) {
			
			isFileExits = false;
			
			try {
				FileOutputStream fos = new FileOutputStream(outputFile);
				fos.close();
				
			} catch (FileNotFoundException e ) {
				Log.i("!!!!!!!!!!", "File create failed error : " + e.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			isFileExits = true;
		}
		
	}
	
	public FileController( File appPath, String fileName  ) {
		
//		File file = context.getDir("appDir", Activity.MODE_PRIVATE);
//		File file = Environment.getExternalStorageDirectory().getAbsoluteFile();
//		String appPath = file.getAbsolutePath();
//		String appPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		
		outputFile = new File(appPath + "/" + fileName );

		/*폴더가 없으면 생성시킴*/
		if( !appPath.exists() ) {
			appPath.mkdirs();
		}
		
		/* 파일이 없으면 생성시킴. */
		if( !outputFile.exists() ) {
			
			isFileExits = false;
			
			try {
				FileOutputStream fos = new FileOutputStream(outputFile);
				fos.close();
				
			} catch (FileNotFoundException e ) {
				Log.i("!!!!!!!!!!", "File create failed error : " + e.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else {
			isFileExits = true;
		}
		
	}
	
	/**
	 * 파일 존재 여부 리턴
	 * @return
	 */
	public boolean isFileExits() {
		return isFileExits;
	}
	
	public byte[] readFileByte(int byteBufferSize) {
		
		byte[] readFileByte = new byte[byteBufferSize];
		FileInputStream fin = null;
		
		try {
			fin = new FileInputStream(outputFile);
			
		} catch ( Exception e) {
			Log.i("FileError", e.toString() );
		}
		
		int temp = 0;
		int flag = 0;
		
		try {
			
			while( ( temp = fin.read() ) != -1 ) {
				readFileByte[flag++] = (byte)temp; 
			}
			fin.close();
			
		} catch( Exception e) {
			
		}
		
		return readFileByte;
	}
	
	public boolean writeFileByte( byte[] inputByteAr ) {
		
		boolean isWriteSuccess = false;
		FileOutputStream fos;
		
		try {
			fos = new FileOutputStream(outputFile);
			
			fos.write( inputByteAr );
			fos.flush();
			
			fos.close();
			
			isWriteSuccess = true;
			
		} catch (FileNotFoundException e) {
			Log.i("!!!!!!!!!!", "File write error : " + e.toString());
			isWriteSuccess =  false;
			
		} catch (IOException e) {
			Log.i("!!!!!!!!!!", "File write error make stream : " + e.toString());
			isWriteSuccess =  false;
		}
		
//		Log.i("Test" , String.valueOf( isWriteSuccess));
		
		return isWriteSuccess;
	}
	
	/**
	 * 문자열 저장
	 * @param inputString
	 * @return
	 */
	public boolean writeFileByString( String inputString ) {
		
		boolean isWriteSuccess = false;
		
		
		try {
			
			PrintWriter pw = new PrintWriter( outputFile );
			pw.println( inputString );
			pw.flush();
			
			pw.close();
			
			isWriteSuccess = true;
			
		} catch (FileNotFoundException e) {
			Log.i("!!!!!!!!!!", "File write error : " + e.toString());
			isWriteSuccess =  false;
			
		} catch (IOException e) {
			Log.i("!!!!!!!!!!", "File write error make stream : " + e.toString());
			isWriteSuccess =  false;
		}
		
//		Log.i("Test" , String.valueOf( isWriteSuccess));
		
		return isWriteSuccess;
	}
	
	/**
	 * 문자열 읽어오기
	 * @param byteBufferSize
	 * @return
	 */
	public String readFileByString() {
		
		BufferedReader fin = null;
		String readString = new String();
		
		try {
			fin = new BufferedReader( new FileReader(  outputFile )  );
			
		} catch ( Exception e) {
			Log.i("FileError", e.toString() );
		}
		
		String temp = new String();
		int flag = 0;
		
		try {
			
			while( ( temp = fin.readLine() ) != null ) {
				readString +=  temp;
				readString += "\r\n";
			}
			fin.close();
			
		} catch( Exception e) {
			
		}
		
		return readString;
	}
	
	/**
	 * 지정된 파일 읽어와서 String으로 반환
	 * @return
	 */
	public String readFile() {
		
		String readFileString = new String();
		
		try {
			FileInputStream fin = new FileInputStream(outputFile);
			InputStreamReader is = new InputStreamReader(fin);
			BufferedReader br = new BufferedReader(is);
			
			String tempData = new String(); 
			
			while ( (tempData = br.readLine()) != null ) {
				readFileString += tempData;
			}
			fin.close();
			
		} catch (FileNotFoundException e) {
			Log.i("!!!!!!!!!!", "File open error : " + e.toString());
			
		} catch (IOException e) {
			Log.i("!!!!!!!!!!", "File open error and to byte : " + e.toString());

		} 
		
		return readFileString;
		
	}
	
	/**
	 * 스트링을 객체 생성시 지정된 파일로 저장
	 * @param setDataString 저장할 문자열
	 * @return boolean 성공/실패 여부
	 */
	public boolean saveFile( String setDataString ) {
		
		boolean isWriteSuccess = false;
		FileOutputStream fos;
		
		try {
			fos = new FileOutputStream(outputFile);
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(setDataString);
			bw.flush();
			
			fos.close();
			bw.close();
			
			isWriteSuccess =  true;
		} catch (FileNotFoundException e) {
			Log.i("!!!!!!!!!!", "File write error : " + e.toString());
			isWriteSuccess =  false;
			
		} catch (IOException e) {
			Log.i("!!!!!!!!!!", "File write error make stream : " + e.toString());
			isWriteSuccess =  false;
			
		}
		
		return isWriteSuccess;
	}
	
}
