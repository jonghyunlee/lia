package com.topperji.util;

public class Util {

	/**
	 * ���Ʈ
	 * @param array
	 * @param left
	 * @param right
	 * @return
	 */
	public static float[] q_sort( float[] array, int left, int right ) {
		
		int pointLeft = left;
		int pointRight = right;
		float pivot = array[left];
		float tmp = 0;
		
		while( pointLeft < pointRight ) {
			
			while( array[pointRight] > pivot && pointLeft < pointRight ) pointRight--;
			while( array[pointLeft] <= pivot && pointLeft < pointRight ) pointLeft++;
			
			tmp = array[pointLeft];
			array[pointLeft] = array[pointRight];
			array[pointRight] = tmp;
			
		}
		array[left] = array[pointLeft]; 
		array[pointLeft] = pivot;
		
		if( pivot > array[left] ) {
			array = q_sort( array, left, pointLeft-1 );
		}
		
		if( pivot < array[right] ) {
			array = q_sort( array, pointLeft+1, right );
		}
		
		
		return array;
	}

}
