package com.topperji.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class UtilsAboutString {
	
	
	/**
	 * DateTime 형의 시간에 대해서 시간단위에 대해서 더하는 경우 경우 날짜 변동을 자동으로 계산해서 출력하는 메소드
	 * @param dateTime 변환하고자 하는 소스 dateTime . "YYYYMMDDhhmmss" 형태
	 * @param hourPlus dateTime에서 더하고자 하는 시간
	 * @return	"YYYYMMDDhhmmss" 형태
	 */
	public static String getDateTimeByPlus( String dateTime, int hourPlus ) {
		
		String returnDateTime = new String();
		
		int subDate = Integer.parseInt( dateTime.substring( 0, 8) );
		int subTimeHour = Integer.parseInt( dateTime.substring( 8, 10) );
		
		int subYear = Integer.parseInt( dateTime.substring( 0, 4) );
		int subMonth = Integer.parseInt( dateTime.substring( 4, 6) );
		int subDay = Integer.parseInt( dateTime.substring( 6, 8) );
		
		GregorianCalendar today = new GregorianCalendar( subYear, subMonth-1, 1);
		int maxday = today.getActualMaximum( Calendar.DAY_OF_MONTH );
		
		
		if( subMonth <= 12 && subDay <= maxday && subTimeHour < 24 ) {
		
			/*hour���곕Ⅸ �좎쭨 怨꾩궛 */
			int pointDayMonth = maxday;
			
			int newYear = subYear;
			int newMonth = subMonth;
			int newDay = subDay + ( subTimeHour + hourPlus )/24;
			int newHour = ( subTimeHour + hourPlus )%24;
			
			int dayForCounting = newDay;
			
			while( dayForCounting > pointDayMonth ) {
				
				newMonth++;
				
				if( newMonth > 12 ) {
					newYear++;
					newMonth = 1;
					
				}
				
				dayForCounting -= pointDayMonth;
				
				GregorianCalendar pointDay = new GregorianCalendar( newYear, newMonth-1, 1);
				pointDayMonth = pointDay.getActualMaximum( Calendar.DAY_OF_MONTH );
			}
			
			returnDateTime =  String.valueOf( newYear );
			returnDateTime +=  String.valueOf( String.format("%02d", newMonth ) );
			returnDateTime +=  String.valueOf( String.format("%02d", dayForCounting  ) );
			returnDateTime +=  String.valueOf( String.format("%02d", newHour) );
			
		} else {
			
			System.out.println("[Error]Invalid dateTime Type. Too much month, day, or hour. ");
			returnDateTime = "0000000000";
			
		}
		
		return returnDateTime;
	}
	
	/**
	 * DateTime 형의 시간에 대해서 시간단위에 대해서 뺄 경우 날짜 변동을 자동으로 계산해서 출력하는 메소드
	 * @param dateTime 변환하고자 하는 소스 dateTime . "YYYYMMDDhhmmss" 형태
	 * @param hourMinus dateTime에서 빼고자 하는 시간
	 * @return	"YYYYMMDDhhmmss" 형태
	 */
	public static String getDateTimeByMinus( String dateTime, int hourMinus ) {
		
		String returnDateTime = new String();
		
		int subDate = Integer.parseInt( dateTime.substring( 0, 8) );
		int subTimeHour = Integer.parseInt( dateTime.substring( 8, 10) );
		
		int subYear = Integer.parseInt( dateTime.substring( 0, 4) );
		int subMonth = Integer.parseInt( dateTime.substring( 4, 6) );
		int subDay = Integer.parseInt( dateTime.substring( 6, 8) );
		
		GregorianCalendar today = new GregorianCalendar( subYear, subMonth-1, 1);
		int maxday = today.getActualMaximum( Calendar.DAY_OF_MONTH );
		
		
		if( subMonth <= 12 && subDay <= maxday && subTimeHour < 24 ) {
		
			
			/*hour���곕Ⅸ �좎쭨 怨꾩궛 */
			int pointMonth = subMonth;
			
			int newYear = subYear;
			int newMonth = subMonth;
			int newDay = 0;
			int newHour = 0;
			
			if( subTimeHour - hourMinus < 0 ) {
				//newDay = subDay - (hourMinus - subTimeHour + 24)/24;
				newDay = subDay - ( (hourMinus-1-subTimeHour)/24 + 1 );
				
				if(  subTimeHour - hourMinus%24 < 0 ) {
					newHour = subTimeHour + ( 24 - hourMinus%24);
					
				} else {
					newHour = subTimeHour - hourMinus%24;
				}
				
			} else {
				newDay = subDay;
				newHour = subTimeHour - hourMinus;
			}
			
			int dayForCounting = newDay;
			
			while( newDay < 1) {
				
				
				GregorianCalendar pointDay = new GregorianCalendar( newYear, --newMonth-1, 1);
				int dayOfpointMonth = pointDay.getActualMaximum( Calendar.DAY_OF_MONTH );
				
				newDay += dayOfpointMonth;
				
				if( newMonth < 1 ) {
					newYear--;
					newMonth = 12;
				}
				
			}
			
			returnDateTime =  String.valueOf( newYear );
			returnDateTime +=  String.valueOf( String.format("%02d", newMonth ) );
			returnDateTime +=  String.valueOf( String.format("%02d", newDay  ) );
			returnDateTime +=  String.valueOf( String.format("%02d", newHour) );
			
		} else {
			
			System.out.println("[Error]Invalid dateTime Type. Too much month, day, or hour. ");
			returnDateTime = "0000000000";
			
		}
		
		return returnDateTime;
	}
	
	/*�꾩옱 �좎쭨, �쒓컙 �삳뒗 �⑥닔 */
	public static String getDateTime() {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		
		return dateFormat.format( now.getTime() ) ;
	}
	
	public static String getNowDate() {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		
		return dateFormat.format( now.getTime()) ;
	}
	
	public static String getPastDay(int days) {
		Calendar now = Calendar.getInstance();
		now.add( Calendar.DATE, days);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		
		return dateFormat.format( now.getTime()) ;
	}
	
	public static String getNowTime() {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("HHmmss");
		
		return dateFormat.format( now.getTime()) ;
	}
	
	public static String getDateTimeCustomType( String formatString ) {
		Calendar now = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatString);
		
		return dateFormat.format( now.getTime() ) ;
	}
	
	/**
	 * Hex String을 정수 형태의 int로 반환
	 * @param hexString "0xFF"와 같은 16진수 형태의 스트링
	 * @return 변환된 정수값
	 */
	public static int stringHexToDec(String hexString) {
		
		int result=0;
		HashMap<String, Integer> hm = new HashMap<String, Integer>();
		
		hm.put("0", 0);
		hm.put("1", 1);
		hm.put("2", 2);
		hm.put("3", 3);
		hm.put("4", 4);
		hm.put("5", 5);
		hm.put("6", 6);
		hm.put("7", 7);
		hm.put("8", 8);
		hm.put("9", 9);
		hm.put("A", 10);
		hm.put("B", 11);
		hm.put("C", 12);
		hm.put("D", 13);
		hm.put("E", 14);
		hm.put("F", 15);
		hm.put("a", 10);
		hm.put("b", 11);
		hm.put("c", 12);
		hm.put("d", 13);
		hm.put("e", 14);
		hm.put("f", 15);
		
		String[] tempHexString = hexString.split("x");
		int point = tempHexString.length - 1;
		
		for( int i = 0; i < tempHexString[point].length(); i++ ) {
			result += hm.get(String.valueOf(tempHexString[point].charAt(i)) ) * Math.pow(16, (tempHexString[point].length()-1-i) );
		}
		
		
		return result;
	}
	
	/**
	 * 입력받은 스트링으로부터 기호를 공백으로 대체
	 * @param inputStr 변환할 스트링
	 * @return 기호가 공백으로 변환된 스트링
	 */
	public static String replaceSimbolChar( String inputStr ) {
		
		String[] arRemoveChar = {"=", "/", "\"", "<", ">", "-", "_", ".", "*", ")", "(", "[", "]", "{", "}", ",", ".", "?", "@", "!", "$", "%", "^", "&", "+", ";", ":", "'", "#" };
		
		for( int i=0; i< arRemoveChar.length; i++ ) {
			inputStr = inputStr.replace( arRemoveChar[i], " ");
		}
		
		return inputStr;
	}
	/**
	 * 입력받은 스트링에서 숫자를 포함하고 있는지 여부를 파악하는 메소드
	 * @param inpuStr 숫자 포함여부를 판단할 스트링
	 * @return 판단 결과에 대한 boolean - 숫자가 포함되어 있으면 true
	 */
	public static boolean containNumberChk( String inpuStr ) {
		
		boolean result = false;
		int flag = 0;
		
		for( int i=0; i < 10; i++ ) {
			
			if( inpuStr.contains( String.valueOf(i) ) )  {
				flag++;
			} 
		}
		
		if( flag > 0 ) {
			result = true;
		} else {
			result = false;
		}
		
		return result;
		
	}

}
