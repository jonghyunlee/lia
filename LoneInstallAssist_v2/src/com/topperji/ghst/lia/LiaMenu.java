package com.topperji.ghst.lia;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.client.ClientProtocolException;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ftdi.j2xx.D2xxManager;
import com.topperji.ftdiController.FtdiController;
import com.topperji.ftdiController.GwMessageComm;
import com.topperji.httpClient.HttpMultiPartPost;

public class LiaMenu extends Activity implements OnClickListener {
	
	Button btnConnection;
	Button btnGwInfo;
	Button btnGwWeekActive;
	Button btnGwFirm;
	
	public EditText editTextConnection;
	
	LinearLayout lLayOutControlBtnGroup;
	
	ImageView imgConn;
	
	FtdiController ftdi = FtdiController.getInstance(this);;
	
	Thread conChkThread;
	ConnectCheckerRunnable ccr;
	
	HttpMultiPartPost httpMultiPartPost;
	ArBeanDTMF arBeanDTMF;
	
	boolean btnConFlag = false; 
	
	boolean expirationEnable = false;
	
	GwMessageComm gmc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lia_menu);
		
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
		
		arBeanDTMF = ArBeanDTMF.getInstance();
		
		/*오브젝트 초기화*/
		btnConnection = (Button)findViewById(R.id.btn_connection);
		btnGwInfo = (Button)findViewById(R.id.btn_gw_state);
		btnGwWeekActive = (Button)findViewById(R.id.btn_gw_7day_active);
		btnGwFirm = (Button)findViewById(R.id.btn_gw_firm);
		
		editTextConnection = (EditText)findViewById(R.id.et_connection);
		editTextConnection.setFocusable(false);
		editTextConnection.setClickable(false);
		
		imgConn = (ImageView)findViewById(R.id.img_conn);
		
		lLayOutControlBtnGroup = (LinearLayout)findViewById(R.id.layout_control_btn_group);
		imgConn.setImageResource( R.drawable.conn_0);
		
		processServerData();	//서버 설정정보 가져오기
		
		setControlGroup( true );	//컨트롤 버튼 그룹 비활성화
		
		/*리스너 등록*/
		btnConnection.setOnClickListener(this);
		btnGwInfo.setOnClickListener(this);
		btnGwWeekActive.setOnClickListener(this);
		btnGwFirm.setOnClickListener(this);
		
		ccr = new ConnectCheckerRunnable( ftdi, this, handler);
		conChkThread = new Thread( ccr );
		conChkThread.start();
		
		 gmc = new GwMessageComm(this, ftdi, handler );
		 
		 startActivity(new Intent(this, NewLiaMenu.class));
		 finish();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();

		ccr.closeRun();
		
		if( ftdi.isConnected() ) {
			ftdi.closeDevice();
		}
		
		ActivityManager aManager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
		aManager.restartPackage( getPackageName() );
		
	}

	public void processServerData() {
		
		TextView tv_2 = (TextView)findViewById(R.id.et_gw_mac );
		
		HttpMultiPartPost hmpp = new HttpMultiPartPost();
		
		hmpp.setServerUrl("http://222.122.128.21/M2M_5TH_LIA/callInterface/liaCall_set.php");
		hmpp.setKey("XBSAT0c12CDSDSE998923jjfnmNSCDnj93hjfndkn929njdsnlnfvjsdfnlk2n3rn" );
		
		try {
			hmpp.executeMultipartsPost();
			
			String[] getData = hmpp.getResponse().split("<br>");
			
			arBeanDTMF.expiration = getData[0].split("<value>")[1];
			arBeanDTMF.m2mHttpUrl = getData[1].split("<value>")[1];
			arBeanDTMF.m2mIpdapAddr = getData[2].split("<value>")[1];
			arBeanDTMF.m2mIpdapPort = Integer.parseInt( getData[3].split("<value>")[1].trim() );
			
		} catch (ClientProtocolException e) {
			Toast.makeText(this, "서버 설정정보를 받아올 수 없습니다. 서버를 확인해보시기 바랍니다.", Toast.LENGTH_SHORT).show();
			
		} catch (IOException e) {
			Toast.makeText(this, "서버 설정정보를 받아올 수 없습니다. IOException. ", Toast.LENGTH_SHORT).show();
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lia_menu, menu);
		return true;
	}
	
	/**
	 * 메인메뉴는 커넥션이 활성화 되었을때만 보인다.
	 * @param enable
	 */
	public void setControlGroup(boolean enable) {
//		enable= true;
		
		if( enable ) {
//			lLayOutControlBtnGroup.setVisibility( Button.VISIBLE );
			btnGwInfo.setVisibility( Button.VISIBLE );
			btnGwWeekActive.setVisibility( Button.VISIBLE );
//			btnGwFirm.setVisibility( Button.VISIBLE );
			
		} else {
//			lLayOutControlBtnGroup.setVisibility( Button.INVISIBLE );
			btnGwInfo.setVisibility( Button.INVISIBLE );
			btnGwWeekActive.setVisibility( Button.INVISIBLE );
			btnGwFirm.setVisibility( Button.INVISIBLE );
			
		}
		
		btnGwInfo.setClickable( enable );
		btnGwWeekActive.setClickable( enable );
		btnGwFirm.setClickable( enable );
		
	}
	
	/**
	 * 
	 * @param sendMessage
	 * @param recieveMessage
	 * @return
	 */
	public void getSerialConnCheck( String sendMessage, String recieveMessage ) {
		
		boolean result = false; 
		
		ftdi.sendMessage( sendMessage );
		
		Thread tr = new Thread( new SerialConCheck( handler, sendMessage, recieveMessage ) );
		tr.start();
		
	}

	@Override
	public void onClick(View v) {
		
		if( v.getId() == R.id.btn_connection ) {
			startActivity(new Intent(this, NewLiaMenu.class));
//			
//			processServerData();
//			
//			long time = System.currentTimeMillis();
//			Date date = new Date(time);
//			SimpleDateFormat sdg = new SimpleDateFormat("yyyyMMddHHmmss");
//			
//			//Toast.makeText(this, arBeanDTMF.expiration  + " : " + sdg.format(date).toString(), Toast.LENGTH_SHORT ).show();
//			
//			if( Long.parseLong( arBeanDTMF.expiration ) >= Long.parseLong( sdg.format(date).toString() ) ) { 	//기간 이내면
//				expirationEnable = true;
//				
//			} else {
//				expirationEnable = false;
//			}
//			
//			if( expirationEnable ) {
//				int i = ftdi.getDeviceList();
//				
//				if( i > 0 ) {	//USB가 인식된 경우
//					
//					if( !ftdi.isConnected() ) {		//연결 시도
//						ftdi.setDeviceInfo( 0, D2xxManager.FT_BITMODE_RESET , 115200, D2xxManager.FT_DATA_BITS_8, D2xxManager.FT_FLOW_NONE);
//						
//						imgConn.setImageResource( R.drawable.conn_1 );
//						
//						ccr.resumeRun();
//						
//						editTextConnection.setText( "USB는 연결확인. 게이트웨이 연결상태 확인중..." );
//						
//						/*시리얼까지 연결됐는지 확인하는 루틴.*/
//						getSerialConnCheck( "*2#", "*0#" );
//						
//						btnConnection.setText( "연결끊기");
//						
//					} else {
//						Toast.makeText( LiaMenu.this, "연결되어있지만,  연결을 끊겠소. ", Toast.LENGTH_SHORT ).show();
//						ftdi.closeDevice();
//						btnConnection.setText( "장비 연결하기");
//						
//					}
//						
//					
//				} else {	//장비 없음
//					Toast.makeText( LiaMenu.this, "연결된 장비가 없습니다. ", Toast.LENGTH_SHORT ).show();
//					setControlGroup( false );
//					imgConn.setImageResource( R.drawable.conn_0 );
//					
//				}
//				
//			} else {
//				Toast.makeText(this, "앱 사용 기간이 만료되었습니다.", Toast.LENGTH_SHORT ).show();
//				
//			}
//			
//				
		} else if( v.getId() == R.id.btn_gw_state ) {
			
			Intent intent = new Intent( this, GwInfo.class );
			startActivity(intent);
			
			
		} else if( v.getId() == R.id.btn_gw_7day_active ) {
			
			Intent intent = new Intent( this, GwActiveHistory.class );
			startActivity(intent);
			
			
		} else if( v.getId() == R.id.btn_gw_firm ) {
			
			Intent intent = new Intent( this, GwFirmWare.class );
			startActivity(intent);
			
		}
		
	}
	
	/**
	 * 쓰레드 메세지 처리 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == 1 ) {
//				Toast.makeText(  LiaMenu.this, "연결된 장비가 끊어졌습니다. ", Toast.LENGTH_SHORT ).show();
				setControlGroup( false );
				imgConn.setImageResource( R.drawable.conn_0 );
				
				editTextConnection.setText( "연결이 끊어졌습니다. 연결 대기중..." );
				btnConnection.setText( "장비 연결하기");
			}
			
			if( msg.what == 3 ) {
				
				String[] receiveMessage = (String[])msg.obj;
				
				/*커넥션 확인 되었으면 이미지 바꾸고 메세지 변경*/
				
//				for( int i=0; i< receiveMessage.length; i++ ) {
//					Log.i("Lia", "Chk : " + receiveMessage[i] );
//				}
				
				if( receiveMessage[1] != null &&  receiveMessage[1].contains( receiveMessage[2] ) ) {	//센드 메세지가 포함된 경우 - 에코로 포함시켜서 돌아온 경우
					receiveMessage[1] = receiveMessage[1].replace( receiveMessage[2], "" );
//					Log.i("Lia", "포함 된거 같은데 : " + receiveMessage[1] );
					
				}
				
				if( receiveMessage[1] != null &&  receiveMessage[0].equals( receiveMessage[1] ) ) {
					imgConn.setImageResource( R.drawable.conn_2 );
					setControlGroup( true );
					editTextConnection.setText( "게이트웨이가 정상 연결되었습니다" );
					
					//시간정보 송신
					Calendar now = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
					
					byte[] byteDateTime = new byte[12];
					int i = 0;
					 
					for( byte a : dateFormat.format( now.getTime() ).substring(2, 14).getBytes() ) {
						byteDateTime[i++] = (byte) (a - 0x30);
					}
					
					gmc.messageSend( (short) 12, byteDateTime );
					
					editTextConnection.setText( "시간 정보를 송신 합니다." );
					
				} else {
					editTextConnection.setText( "게이트웨이를 연결하거나, 연결되었다면 시리얼모드로 전환하십시오." );
				}
				
				Log.i("Lia", receiveMessage[0] + " : " + receiveMessage[1] );
//				Toast.makeText( LiaMenu.this, "액티비티" + receiveMessage[0] + " : " + receiveMessage[1], Toast.LENGTH_SHORT ).show();
			}
			
    	}
    };
	
    /**
     * 게이트웨이 시리얼포트 커넥션 상태를 체크하는 런에이블 - 실제로 USB 연결상태를 지속적으로 체크한다.
     * @author Topper
     *
     */
    class SerialConCheck implements Runnable {
    	
    	Handler mHandler;
    	String sendMessage;
    	String receiveMessage;
    	
    	boolean enableLoop = true;
    	
    	public SerialConCheck( Handler h, String sendMessage, String receiveMessage ) {
    		mHandler = h;
    		this.sendMessage = sendMessage;
    		this.receiveMessage = receiveMessage;
    	}

		@Override
		public void run() {
			
			enableLoop = true;
			
			int flag = 5;
			
			String[] buffer = new String[3];	//[0] 입력된 리시버메세지, [1]시리얼로부터 받은 리시버 메세지.
			buffer[0] = receiveMessage;
			buffer[2] = sendMessage;
			
			while( enableLoop && flag > 0 ) {
				
				if( ftdi.readBufferGet().toString().contains("*") && ftdi.readBufferGet().toString().contains("#") ) {
					enableLoop = false;
					buffer[1] = ftdi.readBufferGet().toString();
					
//					Log.i("Lia", "게이트웨이 : " + ftdi.readBufferGet().toString() );
					
					ftdi.readBufferClear();		
				}
				
				flag--;
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
			
			Message msg = mHandler.obtainMessage();
			msg.what = 3;
			msg.obj = buffer;
			mHandler.sendMessage(msg);
			
		}
		
		public void closeRun() {
			enableLoop = false;
		}
    	
    }

}
