package com.topperji.ghst.lia;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.Toast;

import com.bmk.SendSocket.IPDAPSendSocket.IPDAPDataInfo;
import com.bmk.SendSocket.IPDAPSendSocket.IPDAPSendSocket;
import com.bmk.SendSocket.common.DefineList;
import com.topperji.ftdiController.FtdiController;
import com.topperji.ftdiController.GwMessageComm;
import com.topperji.ghst.liaProcess.BeanDTMF;
import com.topperji.ghst.liaProcess.ProtocolProcessor;
import com.topperji.m2m.CreateDTMFFrame;
import com.topperji.m2m.GHSTUtil;

public class GwFirmWare extends Activity implements OnClickListener {
	
	ImageView imgViewState;
	EditText etActiveHistory;
	EditText etInfoState;
	TabHost tabHost;
	TabHost.TabSpec spec;
	Resources res;
	
	Button btnLoadActive;
	
	CreateDTMFFrame createDtmfFrame = new CreateDTMFFrame();
	
	FtdiController ftdi;
	
	GwMessageComm gmc;
	
	ArrayList<BeanDTMF> arBeanDTMF;
	
	int flag = 0;
	
	int flagMessageTypeGet = 0; // 메세지별 받았는지 여부 확인 변수 0: 하나도 안받음, 1: 맥정보까지 받음, 2:설정정보까지 받음
	
	GHSTUtil ghstUtil = new GHSTUtil();
	
	boolean isActiveHistoryWeek = false;
	ArrayList<String> arWeekData;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gw_firmware);
		
		ftdi = FtdiController.getInstance(this);
		
		res = getResources(); //아이콘 이미지를 위한 리소스화
		
		btnLoadActive = (Button)findViewById(R.id.btn_gw_start_firmware);
		btnLoadActive.setOnClickListener( this);
        
		etActiveHistory = (EditText)findViewById(R.id.et_firm);
		etInfoState = (EditText)findViewById(R.id.et_state_firmware);
		
    	gmc = new GwMessageComm(this, ftdi, handler );
		
    	arWeekData = new ArrayList<String>();
    	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gw_info, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		
		if( v.getId() == R.id.btn_gw_get_active ) {
			
			etActiveHistory.setText("");
			btnLoadActive.setClickable(false);
			btnLoadActive.setFocusable(false);
			btnLoadActive.setText("데이터 송수신중!!");
			
			if( ftdi.isConnected() ) {
				Toast.makeText(this, "게이트웨이 정보를 가져오고 있습니다.", Toast.LENGTH_SHORT ).show();
				
				etInfoState.setText( "1. 데이터 송수신중.." );
				
				flagMessageTypeGet = 1;
				
				gmc.messageSend( (short) 16, null );
				
			} else {
			
			}
		}
	}
	
	synchronized public boolean sendToServer(byte[] headFrame, ArrayList<byte[]> arDataFrame ) {
		
		IPDAPSendSocket iPDAPSendSocket = null;
		
		try {

			iPDAPSendSocket = new IPDAPSendSocket("222.122.128.45",14500 ); 
			
			byte Link[]; 
			// 연결 확인 절차
			{
				Link = new byte[DefineList.LINK_SIZE];
				Link = iPDAPSendSocket.SocketReadLink(  Link.length);
			}
			
			// 헤더 프래임을 전달
			{
				iPDAPSendSocket.SocketWrite( headFrame); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
			}
			
			//데이터 프래임 전달 - 데이터양이 2개 이상일 경우는 루프를 돌린다 
			for( byte[] dataFrame : arDataFrame ){
				iPDAPSendSocket.SocketWrite( dataFrame ); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
			}
			
			
			// 받을 데이터 확인
			LinkedList<IPDAPDataInfo> list = new LinkedList<IPDAPDataInfo>();
			list = iPDAPSendSocket.readIPDAPSaveData();
			
			// 받은 데이터 표시 
			int count = 0;
			if( list == null ){
				Log.i("Lia","NO DATA");
			}
			for( IPDAPDataInfo obj : list ){
				
				Log.i("Lia",++count +" HEAD : " + ghstUtil.GetStringFromByte(obj.getHead()));
				if( obj.getDatList() != null )
				for( byte[] headData : obj.getDatList()){
					Log.i("Lia",count +" DATA : " + ghstUtil.GetStringFromByte(headData));
				} 
			}
			
			iPDAPSendSocket.close();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();

		}
		
		return iPDAPSendSocket.isClosed();
	}
	
	/**
	 * 데이터 프레임으로부터 맥 어드레스 파싱
	 * @param macPieceofStrData
	 * @return
	 */
	public String getMacFromData( String macPieceofStrData ) {
		
		String macStr = new String();
		
		String tempVal = new String();
		
		for( int i=0; i< macPieceofStrData.length(); i++ ) {
			
			if( i%3 != 2 ) {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
			
			} else {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
				
				macStr += String.format("%02x", Integer.parseInt( tempVal )  );
				tempVal = "";
			}
			
		}
		
		return macStr;
	}
	
	public void processGetData( BeanDTMF beans ) {
		
		if( beans.strHID.equals( "17" ) ) {	// 센서 정보일 경우
			
			isActiveHistoryWeek = true;
			
		} 
			
//		arBeanDTMF.clear();
		
	}
	
	/**
	 * 메세지 리더 콜백을 위한 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == 10 ) {
				
				etInfoState.append("\n2. GW로부터 데이터 수신 완료 및 처리중.");
				
				ArrayList<String> getDataList = (ArrayList<String>)msg.obj;
				
				//배열 데이터 처리
				StringBuffer str = new StringBuffer();
				
				Iterator<String> it = getDataList.iterator();
				
				while( it.hasNext() ) {
					
					String temp = it.next();
					
					if( ProtocolProcessor.checkDTMFFrame(temp ) ) {
						
						str.append( temp + "\n" );
//						arBeanDTMF.add( ProtocolProcessor.getBeanFromDTMFFrame( temp ) );
						
						processGetData( ProtocolProcessor.getBeanFromDTMFFrame( temp ) );
						
					} else if( temp.contains("#") ) {
						
						if( isActiveHistoryWeek ) {
							arWeekData.add(temp);
						}
						
						
					} else {
						etInfoState.append("\n** 데이터 수신 에러.");
					}
					
				}
				
				Log.i("Lia", "데이터프레임 사이즈 : " + String.valueOf( arWeekData.size() ) );
				
				etInfoState.append("\n3. 수신데이터 처리완료.");
				
				Toast.makeText( GwFirmWare.this, "게이트웨이 데이터 처리완료 및 서버 데이터 분석 " , Toast.LENGTH_SHORT ).show();
				
				etInfoState.append("\n4. M2M 서버 접속 및 동일 GW 맥어드레스 정보 수신.");
				
				etInfoState.append("\n5. 처리 완료.");
				
//				Toast.makeText( context, receiveMessage , Toast.LENGTH_SHORT ).show();
			
			} else if( msg.what == 11 ) {	//메세지 수신중
				etInfoState.append("▼");
			}
			
			
			
    	}
    };
    
}