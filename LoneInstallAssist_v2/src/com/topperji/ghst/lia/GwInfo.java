package com.topperji.ghst.lia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.client.ClientProtocolException;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.topperji.ftdiController.FtdiController;
import com.topperji.ftdiController.GwMessageComm;
import com.topperji.ghst.liaProcess.BeanDTMF;
import com.topperji.ghst.liaProcess.ProtocolProcessor;
import com.topperji.httpClient.HttpMultiPartPost;
import com.topperji.m2m.CreateDTMFFrame;

public class GwInfo extends Activity implements OnClickListener {
	
	ImageView imgViewState;
	EditText etInfoState;
	
	TabHost tabHost;
	TabHost.TabSpec spec;
	Resources res;
	
	Button btnLoadData;
	Button btnGwOpenIntent;
	
	CreateDTMFFrame createDtmfFrame = new CreateDTMFFrame();
	
	FtdiController ftdi;
	
	GwMessageComm gmc;
	
	ArBeanDTMF arBeanDTMF;
	
	int flag = 0;
	
	int flagMessageTypeGet = 0; // 메세지별 받았는지 여부 확인 변수 0: 하나도 안받음, 1: 맥정보까지 받음, 2:설정정보까지 받음
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gw_info);
		
		ftdi = FtdiController.getInstance(this);
		
		res = getResources(); //아이콘 이미지를 위한 리소스화
		
//		arBeanDTMF = new ArrayList<BeanDTMF>();
		arBeanDTMF = ArBeanDTMF.getInstance();
		
		Toast.makeText(this, arBeanDTMF.m2mHttpUrl , Toast.LENGTH_SHORT ).show();
        
        /*탭 셋팅*/
        tabHost = (TabHost)findViewById(R.id.tabhost);
        tabHost.setup();
        
        spec = tabHost.newTabSpec("G/W Info");
        spec.setContent( R.id.table_gw_info );
        spec.setIndicator("게이트웨이 정보" );	//res.getDrawable(R.drawable.tab_ets)
        tabHost.addTab(spec);
        
        spec = tabHost.newTabSpec("Server Info");
        spec.setContent( R.id.tab_server_info );
        spec.setIndicator("GW맥과 일치하는 서버정보" );	//res.getDrawable(R.drawable.tab_ets)
        tabHost.addTab(spec);
        
    	/*기타 위젯 셋팅*/
    	btnLoadData = (Button)findViewById(R.id.btn_refresh);
    	btnLoadData.setOnClickListener(this);
    	
    	btnGwOpenIntent = (Button)findViewById(R.id.btn_gw_open_intent);
    	btnGwOpenIntent.setOnClickListener(this);
    	
    	etInfoState = (EditText)findViewById(R.id.et_state_gw_info);
    	etInfoState.setFocusable(false);
//    	etInfoState.setClickable(false);
    	
    	gmc = new GwMessageComm(this, ftdi, handler );
    	
    	imgViewState = (ImageView)findViewById(R.id.img_state);
    	imgViewState.setImageResource(R.drawable.open_0 );
    	
    	/*싱글톤 저장 정보 셋팅*/
		Iterator<String> it = arBeanDTMF.iterator();
		while( it.hasNext() ) {
			processGetData( ProtocolProcessor.getBeanFromDTMFFrame( it.next() ) );
		}
    	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gw_info, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		
		if( v.getId() == R.id.btn_refresh ) {
			
			if( ftdi.isConnected() ) {
				Toast.makeText(this, "게이트웨이 정보를 가져오고 있습니다.", Toast.LENGTH_SHORT ).show();
				
				etInfoState.setTextColor( 0xff000000 );
				etInfoState.setText( "1. 데이터 송수신중.." );
				
				flagMessageTypeGet = 1;
				
				byte[] data = {1,1,1,1,1,1};
				gmc.messageSend( (short) 04, data );
				
			} else {
				Toast.makeText(this, "장비가 끊어져 있음.", Toast.LENGTH_SHORT ).show();
			}
			
		} else if( v.getId() == R.id.btn_gw_open_intent) {
			
			if( ((TextView)findViewById(R.id.tv_sensor_1_1 ) ).getText().toString().trim().equals("") ) {
				Toast.makeText(this, "로드된 장비 정보가 없습니다.게이트웨이 정보를 먼저 읽어오세요.", Toast.LENGTH_SHORT ).show();
				
			} else {
				
				TextView[] tv_mac = new TextView[8];
				tv_mac[0] = (TextView)findViewById(R.id.tv_sensor_2_1 );	//활동1
				tv_mac[1] = (TextView)findViewById(R.id.tv_sensor_3_1 );	//활동2
				tv_mac[2] = (TextView)findViewById(R.id.tv_sensor_4_1 );	//화재1
				tv_mac[3] = (TextView)findViewById(R.id.tv_sensor_5_1 );	//화재2
				tv_mac[4] = (TextView)findViewById(R.id.tv_sensor_6_1 );	//가스1
				tv_mac[5] = (TextView)findViewById(R.id.tv_sensor_7_1 );	//가스2
				tv_mac[6] = (TextView)findViewById(R.id.tv_sensor_8_1 );	//도어
				tv_mac[7] = (TextView)findViewById(R.id.tv_sensor_1_1 );	//게이트웨이
				
				Intent intent = new Intent( this, GwOpen.class );
				intent.putExtra("0x01", tv_mac[7].getText() );
				intent.putExtra("0x11", tv_mac[0].getText() );
				intent.putExtra("0x12", tv_mac[1].getText() );
				intent.putExtra("0x21", tv_mac[2].getText() );
				intent.putExtra("0x22", tv_mac[3].getText() );
				intent.putExtra("0x31", tv_mac[4].getText() );
				intent.putExtra("0x32", tv_mac[5].getText() );
				intent.putExtra("0x41", tv_mac[6].getText() );
				
				startActivity(intent);
			}
			
			
		}
		
		
	}
	
	public void processGetData( BeanDTMF beans ) {
		
		if( beans.strHID.equals( "02" ) ) {	// 센서 정보일 경우
			
			if( beans.strData.substring(0, 2).equals("01") ) {	//게이트웨이
				TextView tv = (TextView)findViewById(R.id.tv_sensor_1_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			} else if( beans.strData.substring(0, 2).equals("11") ) {	//활동1
				TextView tv = (TextView)findViewById(R.id.tv_sensor_2_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			} else if( beans.strData.substring(0, 2).equals("12") ) {	//활동2
				TextView tv = (TextView)findViewById(R.id.tv_sensor_3_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			} else if( beans.strData.substring(0, 2).equals("21") ) {	//화재1
				TextView tv = (TextView)findViewById(R.id.tv_sensor_4_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			} else if( beans.strData.substring(0, 2).equals("22") ) {	//화재2
				TextView tv = (TextView)findViewById(R.id.tv_sensor_5_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			} else if( beans.strData.substring(0, 2).equals("31") ) {	//가스1
				TextView tv = (TextView)findViewById(R.id.tv_sensor_6_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			} else if( beans.strData.substring(0, 2).equals("32") ) {	//가스2
				TextView tv = (TextView)findViewById(R.id.tv_sensor_7_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			} else if( beans.strData.substring(0, 2).equals("41") ) {	//도어
				TextView tv = (TextView)findViewById(R.id.tv_sensor_8_1 );
				tv.setText( getMacFromData( beans.strData.substring(2, beans.strData.length() )) );
				
			}
			
		} else if (  beans.strHID.equals( "05" ) ) {	//설정정보 보고 요청결과
			
			if( beans.strData.substring(0, 2).equals("01") ) {	//SID - 01 : 전화번호 설정 정보
				
				int teleNumLength = Integer.parseInt( beans.strData.substring(4, 6) );
				
				String phoneNumber = new String();
				
				for( int i=0; i<teleNumLength; i++ ) {
					phoneNumber +=  beans.strData.substring( i+6, i+7);
				}
				
				if( beans.strData.substring(2, 4).equals("01") ) {	//게이트웨이번호
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_0);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("02") ) {	//상태보고 번호
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_1);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("03") ) {	//원격제어 번호
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_2);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("04") ) {	//다운로더 번호
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_3);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("05") ) {	//지역센터 번호
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_4);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("06") ) {	//119지역 번호
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_5);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("07") ) {	//노인돌보미 번호
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_6);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("08") ) {	//보호자1
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_7);
					tvPhoneGw.setText( phoneNumber );
					
				} else if( beans.strData.substring(2, 4).equals("09") ) {	//보호자2
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_num_8);
					tvPhoneGw.setText( phoneNumber );
					
				}
				
				
			} else if( beans.strData.substring(0, 2).equals("02") ) {	//SID - 02 :  통신주기
				
				String time = new String();
				
				for( int i=0; i<3; i++ ) {
					time +=  beans.strData.substring( i+4, i+5);
				}
				
				if( beans.strData.substring(2, 4).equals("01") ) {	//주기보고 시간
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_cycle);
					tvPhoneGw.setText( Integer.parseInt(time)*10 + " 분" );
					
				} else if( beans.strData.substring(2, 4).equals("02") ) {	//미감지 주기 시간
					TextView tvPhoneGw = (TextView)findViewById(R.id.et_no_active);
					tvPhoneGw.setText(  Integer.parseInt(time)*10  + " 분");
					
				} 
				
				
			} else if( beans.strData.substring(0, 2).equals("03") ) {	//SID - 03 :  강제 착신 응답시간
				
				String time = new String();
				
				for( int i=0; i<3; i++ ) {
					time +=  beans.strData.substring( i+4, i+5);
				}
				
				TextView tvPhoneGw = (TextView)findViewById(R.id.et_force_request);
				tvPhoneGw.setText( Integer.parseInt(time) + " 분" );
					
				
			} else if( beans.strData.substring(0, 2).equals("04") ) {	//SID - 04 :  센서별 맥어드레스
				
				if( beans.strData.substring(2, 4).equals("01") ) {	//게이트웨이
					TextView tv = (TextView)findViewById(R.id.tv_sensor_1_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
					TextView tv_2 = (TextView)findViewById(R.id.et_gw_mac );
					tv_2.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				} else if( beans.strData.substring(2, 4).equals("11") ) {	//활동1
					TextView tv = (TextView)findViewById(R.id.tv_sensor_2_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				} else if( beans.strData.substring(2, 4).equals("12") ) {	//활동2
					TextView tv = (TextView)findViewById(R.id.tv_sensor_3_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				} else if( beans.strData.substring(2, 4).equals("21") ) {	//화재1
					TextView tv = (TextView)findViewById(R.id.tv_sensor_4_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				} else if( beans.strData.substring(2, 4).equals("22") ) {	//화재2
					TextView tv = (TextView)findViewById(R.id.tv_sensor_5_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				} else if( beans.strData.substring(2, 4).equals("31") ) {	//가스1
					TextView tv = (TextView)findViewById(R.id.tv_sensor_6_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				} else if( beans.strData.substring(2, 4).equals("32") ) {	//가스2
					TextView tv = (TextView)findViewById(R.id.tv_sensor_7_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				} else if( beans.strData.substring(2, 4).equals("41") ) {	//도어
					TextView tv = (TextView)findViewById(R.id.tv_sensor_8_1 );
					tv.setText( getMacFromData( beans.strData.substring(4, beans.strData.length() )) );
					
				}
				
			} else if( beans.strData.substring(0, 2).equals("05") ) {	//SID - 05 :  센서 상태
				
				TextView[] tv_bat = new TextView[7];
				tv_bat[0] = (TextView)findViewById(R.id.tv_sensor_2_4 );	//활동1
				tv_bat[1] = (TextView)findViewById(R.id.tv_sensor_3_4 );	//활동2
				tv_bat[2] = (TextView)findViewById(R.id.tv_sensor_4_4 );	//화재1
				tv_bat[3] = (TextView)findViewById(R.id.tv_sensor_5_4 );	//화재2
				tv_bat[4] = (TextView)findViewById(R.id.tv_sensor_6_4 );	//가스1
				tv_bat[5] = (TextView)findViewById(R.id.tv_sensor_7_4 );	//가스2
				tv_bat[6] = (TextView)findViewById(R.id.tv_sensor_8_4 );	//도어
				
				TextView[] tv_comm = new TextView[7];
				tv_comm[0] = (TextView)findViewById(R.id.tv_sensor_2_5 );	//활동1
				tv_comm[1] = (TextView)findViewById(R.id.tv_sensor_3_5 );	//활동2
				tv_comm[2] = (TextView)findViewById(R.id.tv_sensor_4_5 );	//화재1
				tv_comm[3] = (TextView)findViewById(R.id.tv_sensor_5_5 );	//화재2
				tv_comm[4] = (TextView)findViewById(R.id.tv_sensor_6_5 );	//가스1
				tv_comm[5] = (TextView)findViewById(R.id.tv_sensor_7_5 );	//가스2
				tv_comm[6] = (TextView)findViewById(R.id.tv_sensor_8_5 );	//도어
				
				
				for(int i=0; i<7; i++) {
					
					if( beans.strData.substring( i+2, i+3 ).equals("0") ) {
						tv_bat[i].setText("양호");
						tv_comm[i].setText("양호");
						
					} else if( beans.strData.substring( i+2, i+3 ).equals("1") ) {
						tv_bat[i].setText("-");
						tv_comm[i].setText("-");
						
					} else if( beans.strData.substring( i+2, i+3 ).equals("2") ) {
						tv_bat[i].setText("불량");
						tv_comm[i].setText("양호");
						
					} else if( beans.strData.substring( i+2, i+3 ).equals("3") ) {
						tv_bat[i].setText("-");
						tv_comm[i].setText("-");
						
					} else if( beans.strData.substring( i+2, i+3 ).equals("4") ) {
						tv_bat[i].setText("양호");
						tv_comm[i].setText("불량");
						
					} else if( beans.strData.substring( i+2, i+3 ).equals("5") ) {
						tv_bat[i].setText("-");
						tv_comm[i].setText("-");
						
					} else if( beans.strData.substring( i+2, i+3 ).equals("6") ) {
						tv_bat[i].setText("불량");
						tv_comm[i].setText("불량");
						
					} else if( beans.strData.substring( i+2, i+3 ).equals("7") ) {
						tv_bat[i].setText("-");
						tv_comm[i].setText("-");
						
					}
				}
				
			} else if( beans.strData.substring(0, 2).equals("06") ) {	//SID - 06 :  펌웨어 버전
				
				String version = new String();
				
				for( int i=0; i<4; i++ ) {
					version +=  beans.strData.substring( i+4, i+5);
					if( i==1 ) {
						version += ".";
					}
				}
				TextView tvFirm = (TextView)findViewById(R.id.et_firm);
				
				if( beans.strData.substring( 2, 4 ).equals("01") ) {	//코디네이터
					tvFirm.setText( "Codi : " + version );
					
				} else if( beans.strData.substring( 2, 4 ).equals("02") ) {	//게이트웨이
					tvFirm.append( "  /  GW : " + version );
					
				}
				
			}
			
		}
			
//		arBeanDTMF.clear();
		
	}
	
	/**
	 * 센서 코드로부터 센서이름 리턴
	 * @param sensorCode
	 * @return
	 */
	public String getSensorType( String sensorCode ) {
		
		String sensorType = new String();
		
		if( sensorCode.equals("01") ) {
			sensorType = "GW";
		} else if( sensorCode.equals("11") ) {
			sensorType = "활동1";
		} else if( sensorCode.equals("12") ) {
			sensorType = "활동2";
		} else if( sensorCode.equals("21") ) {
			sensorType = "화재1";
		} else if( sensorCode.equals("22") ) {
			sensorType = "화재2";
		} else if( sensorCode.equals("31") ) {
			sensorType = "가스1";
		} else if( sensorCode.equals("32") ) {
			sensorType = "가스2";
		} else if( sensorCode.equals("41") ) {
			sensorType = "외출";
		} else {
			sensorType = "???";
		}
		return sensorType;
	}
	
	/**
	 * 데이터 프레임으로부터 맥 어드레스 파싱
	 * @param macPieceofStrData
	 * @return
	 */
	public String getMacFromData( String macPieceofStrData ) {
		
		String macStr = new String();
		
		String tempVal = new String();
		
		for( int i=0; i< macPieceofStrData.length(); i++ ) {
			
			if( i%3 != 2 ) {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
			
			} else {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
				
				macStr += String.format("%02x", Integer.parseInt( tempVal )  );
				tempVal = "";
			}
			
		}
		
		return macStr;
	}
	
	/**
	 * M2M서버 커넥션 및 받아온 데이터 처리
	 */
	public void processServerData() {
		
		TextView tv_2 = (TextView)findViewById(R.id.et_gw_mac );
		
		HttpMultiPartPost hmpp = new HttpMultiPartPost();
		
		hmpp.setServerUrl( arBeanDTMF.m2mHttpUrl );
		hmpp.addParameter("gwMac", tv_2.getText().toString() );
		
		try {
			hmpp.executeMultipartsPost();
			
		} catch (ClientProtocolException e) {
			etInfoState.setTextColor( 0xffff0000 );
			etInfoState.append("\n  @!! 서버 접속 에려 - ClientProtocolException.");
			
		} catch (IOException e) {
			etInfoState.setTextColor( 0xffff0000 );
			etInfoState.append("\n  @!! 서버 접속 에려 - IOException.");
		}
		
		if( hmpp.getResponse().trim().equals("001")) {
			etInfoState.setTextColor( 0xffff0000 );
			etInfoState.setText("\n**서버에 일치 GW 맥정보 없음!!!.");
			Toast.makeText( GwInfo.this, "일치 GW 맥정보 없음. " , Toast.LENGTH_SHORT ).show();
			
		} else if( hmpp.getResponse().trim().equals("002")) {
			etInfoState.setTextColor( 0xffff0000 );
			etInfoState.setText("\n**서버에 일치 GW 맥정보가 2개 이상 존재!!!.");
			Toast.makeText( GwInfo.this, "일치 GW 맥정보가 2개 이상 존재. " , Toast.LENGTH_SHORT ).show();
			
		} else {
			String[] getData = hmpp.getResponse().split("<br>");
			
			String[] infoGwJoin = getData[0].split("/");
			String[] macAddress = getData[1].split("/");
			
			TextView[] tv_mac = new TextView[8];	//서버 
			tv_mac[0] = (TextView)findViewById(R.id.tv_sensor_2_2 );	//활동1
			tv_mac[1] = (TextView)findViewById(R.id.tv_sensor_3_2 );	//활동2
			tv_mac[2] = (TextView)findViewById(R.id.tv_sensor_4_2 );	//화재1
			tv_mac[3] = (TextView)findViewById(R.id.tv_sensor_5_2 );	//화재2
			tv_mac[4] = (TextView)findViewById(R.id.tv_sensor_6_2 );	//가스1
			tv_mac[5] = (TextView)findViewById(R.id.tv_sensor_7_2 );	//가스2
			tv_mac[6] = (TextView)findViewById(R.id.tv_sensor_8_2 );	//도어
			tv_mac[7] = (TextView)findViewById(R.id.tv_sensor_1_2 );	//게이트웨이
			
			TextView[] tv_mac_gw = new TextView[8];	//장비
			tv_mac_gw[0] = (TextView)findViewById(R.id.tv_sensor_2_1 );	//활동1
			tv_mac_gw[1] = (TextView)findViewById(R.id.tv_sensor_3_1 );	//활동2
			tv_mac_gw[2] = (TextView)findViewById(R.id.tv_sensor_4_1 );	//화재1
			tv_mac_gw[3] = (TextView)findViewById(R.id.tv_sensor_5_1 );	//화재2
			tv_mac_gw[4] = (TextView)findViewById(R.id.tv_sensor_6_1 );	//가스1
			tv_mac_gw[5] = (TextView)findViewById(R.id.tv_sensor_7_1 );	//가스2
			tv_mac_gw[6] = (TextView)findViewById(R.id.tv_sensor_8_1 );	//도어
			tv_mac_gw[7] = (TextView)findViewById(R.id.tv_sensor_1_1 );	//게이트웨이
			
			TextView[] tv_match = new TextView[8];	//일치여부
			tv_match[0] = (TextView)findViewById(R.id.tv_sensor_2_3 );	//활동1
			tv_match[1] = (TextView)findViewById(R.id.tv_sensor_3_3 );	//활동2
			tv_match[2] = (TextView)findViewById(R.id.tv_sensor_4_3 );	//화재1
			tv_match[3] = (TextView)findViewById(R.id.tv_sensor_5_3 );	//화재2
			tv_match[4] = (TextView)findViewById(R.id.tv_sensor_6_3 );	//가스1
			tv_match[5] = (TextView)findViewById(R.id.tv_sensor_7_3 );	//가스2
			tv_match[6] = (TextView)findViewById(R.id.tv_sensor_8_3 );	//도어
			tv_match[7] = (TextView)findViewById(R.id.tv_sensor_1_3 );	//게이트웨이
			
			for( String mac : macAddress ) {
				
				Log.i("Lia", mac );
				
				if( mac.split(":")[0].equals("0x01") ) {
					tv_mac[7].setText( mac.split(":")[1] );
					
				} else if(mac.split(":")[0].equals("0x11") ) {
					tv_mac[0].setText( mac.split(":")[1] );
				} else if(mac.split(":")[0].equals("0x12") ) {
					tv_mac[1].setText( mac.split(":")[1] );
				} else if(mac.split(":")[0].equals("0x21") ) {
					tv_mac[2].setText( mac.split(":")[1] );
				} else if(mac.split(":")[0].equals("0x22") ) {
					tv_mac[3].setText( mac.split(":")[1] );
				} else if(mac.split(":")[0].equals("0x31") ) {
					tv_mac[4].setText( mac.split(":")[1] );
				} else if(mac.split(":")[0].equals("0x32") ) {
					tv_mac[5].setText( mac.split(":")[1] );
				} else if(mac.split(":")[0].equals("0x41") ) {
					tv_mac[6].setText( mac.split(":")[1] );
				}
				
			}
			
			boolean macMatch = true;
			/*장비와 서버 맥어드레스 일치여부*/
			for(int i=0; i<8; i++ ) {
				
				if( tv_mac[i].getText().equals(tv_mac_gw[i].getText() ) ) {
					
					if( tv_mac[i].getText().equals("") && tv_mac_gw[i].getText().equals("") ) {
						tv_match[i].setBackgroundColor( Color.BLACK );
						tv_match[i].setText("-");
						
					} else {
						tv_match[i].setBackgroundColor( Color.GREEN );
						tv_match[i].setTextColor(Color.WHITE);
						tv_match[i].setText("O");
					}
					
				} else {
					tv_match[i].setBackgroundColor( Color.RED );
					tv_match[i].setText("X");
					macMatch = false;
				}
				
			}
			for( String info : infoGwJoin ) {
				Log.i("Lia", info );
			}
			
			Log.i("Lia", infoGwJoin[18] );
			
			/*이미지*/
			if( infoGwJoin[18].equals("8002") ) {
				imgViewState.setImageResource(R.drawable.open_1 );
				
			} else if( infoGwJoin[18].equals("8003") ) {
				imgViewState.setImageResource(R.drawable.open_2 );
				
			} else if( infoGwJoin[18].equals("8008") ) {
				imgViewState.setImageResource(R.drawable.open_3 );
				
			} else if( infoGwJoin[18].equals("8009") ) {
				
				if( macMatch ) {
					imgViewState.setImageResource(R.drawable.open_4 );
				} else {
					imgViewState.setImageResource(R.drawable.open_5 );
				}
				
				
			}
			
			
			Toast.makeText( GwInfo.this, "서버 데이터 분석 완료 " , Toast.LENGTH_SHORT ).show();
			
		}
		
	}
	
	/**
	 * 메세지 리더 콜백을 위한 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == 10 ) {
				
				arBeanDTMF.clear();
				
				etInfoState.append("\n2. GW로부터 데이터 수신 완료 및 처리중.");
				
				ArrayList<String> getDataList = (ArrayList<String>)msg.obj;
				
				//배열 데이터 처리
				StringBuffer str = new StringBuffer();
				
				Iterator<String> it = getDataList.iterator();
				
				while( it.hasNext() ) {
					
					String temp = it.next();
					
					if( ProtocolProcessor.checkDTMFFrame(temp ) ) {
						
						str.append( temp + "\n" );
						arBeanDTMF.add( temp );
						
						processGetData( ProtocolProcessor.getBeanFromDTMFFrame( temp ) );
						
					} else {
						etInfoState.append("\n** 데이터 수신 에러.");
					}
					
				}
				etInfoState.append("\n3. 수신데이터 처리완료.");
				
				Toast.makeText( GwInfo.this, "게이트웨이 데이터 처리완료 및 서버 데이터 분석 " , Toast.LENGTH_SHORT ).show();
				
				etInfoState.append("\n4. M2M 서버 접속 및 동일 GW 맥어드레스 정보 수신.");
				processServerData();
				
				etInfoState.append("\n5. 처리 완료.");
				
//				Toast.makeText( context, receiveMessage , Toast.LENGTH_SHORT ).show();
			
			} else if( msg.what == 11 ) {	//메세지 수신중
				etInfoState.append("▼");
			}
			
    	}
    };
    
}