package com.topperji.ghst.lia;

import java.util.ArrayList;

import com.topperji.ghst.liaProcess.BeanDTMF;

public class ArBeanDTMF extends ArrayList<String> {
	
	private static ArBeanDTMF object = null;
	
	public static String expiration = new String();
	public static String m2mHttpUrl = new String();
	public static String m2mIpdapAddr = new String();
	public static Integer m2mIpdapPort = 0;
	
	
	public static ArBeanDTMF getInstance() {
		
		if( object == null ) {
			object = new ArBeanDTMF();
		}
		
		return object;
		
	}

}
