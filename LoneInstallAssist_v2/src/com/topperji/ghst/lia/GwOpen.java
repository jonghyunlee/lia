package com.topperji.ghst.lia;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TabHost;
import android.widget.Toast;

import com.bmk.SendSocket.IPDAPSendSocket.IPDAPDataInfo;
import com.bmk.SendSocket.IPDAPSendSocket.IPDAPSendSocket;
import com.bmk.SendSocket.common.CreateFrame;
import com.bmk.SendSocket.common.DefineList;
import com.topperji.ftdiController.FtdiController;
import com.topperji.ftdiController.GwMessageComm;
import com.topperji.m2m.CreateDTMFFrame;
import com.topperji.m2m.DefineHeaderFrame;
import com.topperji.m2m.GHSTUtil;
import com.topperji.util.FileController;

public class GwOpen extends Activity implements OnClickListener {
	
	ImageView imgViewState;
	EditText etInfoState;
	EditText etGwNumber;
	EditText etName;
	EditText etAddress;
	TabHost tabHost;
	TabHost.TabSpec spec;
	Resources res;
	
	Button btnLoadData;
	Button btnGwOpen;
	
	CreateDTMFFrame createDtmfFrame = new CreateDTMFFrame();
	
	FtdiController ftdi;
	
	GwMessageComm gmc;
	
	ArBeanDTMF arBeanDTMF;
	ArrayList<String> arMacList;
	
	int flag = 0;
	
	int flagMessageTypeGet = 0; // 메세지별 받았는지 여부 확인 변수 0: 하나도 안받음, 1: 맥정보까지 받음, 2:설정정보까지 받음
	
	GHSTUtil ghstUtil = new GHSTUtil();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gw_open);
		
		ftdi = FtdiController.getInstance(this);
		arBeanDTMF = ArBeanDTMF.getInstance();
		
		res = getResources(); //아이콘 이미지를 위한 리소스화
		
		btnLoadData = (Button)findViewById(R.id.btn_refresh);
		btnLoadData.setOnClickListener( this);
        
		btnGwOpen = (Button)findViewById(R.id.btn_gw_open);
		btnGwOpen.setOnClickListener( this );
    	
		etInfoState = (EditText)findViewById(R.id.et_state_gw_open);
		
		etGwNumber = (EditText)findViewById(R.id.et_gw_number);
//		etGwNumber.setText("07078567856");	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!삭제
		
		etName = (EditText)findViewById(R.id.et_name);
		etAddress = (EditText)findViewById(R.id.et_address);
		
    	gmc = new GwMessageComm(this, ftdi, handler );
		
    	arMacList = new ArrayList<String>();
//    	intent.putExtra("0x01", tv_mac[7].getText() );//본체
//		intent.putExtra("0x11", tv_mac[0].getText() );//활동1
//		intent.putExtra("0x12", tv_mac[1].getText() );
//		intent.putExtra("0x21", tv_mac[2].getText() );
//		intent.putExtra("0x22", tv_mac[3].getText() );
//		intent.putExtra("0x31", tv_mac[4].getText() );
//		intent.putExtra("0x32", tv_mac[5].getText() );
//		intent.putExtra("0x41", tv_mac[6].getText() );
    	arMacList.add(  "01" + this.getIntent().getStringExtra("0x01") );//본체
    	arMacList.add(  "11" + this.getIntent().getStringExtra("0x11") );//활동1
    	arMacList.add(  "12" + this.getIntent().getStringExtra("0x12") );//활동2
    	arMacList.add(  "21" + this.getIntent().getStringExtra("0x21") );//화재1
    	arMacList.add(  "22" + this.getIntent().getStringExtra("0x22") );//화재2
    	arMacList.add(  "31" + this.getIntent().getStringExtra("0x31") );//가스1
    	arMacList.add(  "32" + this.getIntent().getStringExtra("0x32") );//가스2
    	arMacList.add(  "41" + this.getIntent().getStringExtra("0x41") );//도어1
    	
//    	arMacList.add(  "0100158d0000111101" );
//    	arMacList.add(  "1100158d0000111111" );
//    	arMacList.add(  "1200158d0000111112" );
//    	arMacList.add(  "2100158d0000111121" );
//    	arMacList.add(  "22" );
//    	arMacList.add(  "3100158d0000111131" );
//    	arMacList.add(  "32" );
//    	arMacList.add(  "4100158d0000111141" );
	    
    	
    	Log.i("Test", arBeanDTMF.m2mIpdapAddr + " / " + String.valueOf(arBeanDTMF.m2mIpdapPort ) );
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gw_info, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		
		if( v.getId() == R.id.btn_refresh ) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Dialog");
			builder.setNeutralButton("닫기", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
				}
			});
			
			final Cursor c = getContentResolver().query( CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE + " DESC");
			
		    startManagingCursor(c);
		    
		    final ListAdapter LA = new SimpleCursorAdapter(
		    		this, 
		    		android.R.layout.simple_list_item_2, c, 
		    		new String[]{Calls.CACHED_NAME, Calls.NUMBER }, 
		    		new int[] { android.R.id.text1, android.R.id.text2 });

		    builder.setAdapter(LA, new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
					EditText et = (EditText)findViewById( R.id.et_gw_number );
					et.setText( c.getString(3) );
					
				}
			} );
			builder.show();
		
		} else if( v.getId() == R.id.btn_gw_open ) {
			
			AlertDialog.Builder openDialog = new AlertDialog.Builder(this );
			
			openDialog.setTitle("게이트웨이 시리얼모드 확인");
			openDialog.setMessage("게이트웨이가 시리얼 모드인지 다시 확인 바랍니다. \n 시리얼모드가 아니면 시리얼 모드로 전환하고 난 다음 아래 개통시작 버튼을 눌러주십시오. ");
			openDialog.setPositiveButton("개통 시작.", 
					new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							etInfoState.setText("");
							btnGwOpen.setClickable(false);
							btnGwOpen.setFocusable(false);
							btnGwOpen.setText("개통중!!");
							
							/*서버 데이터 전송*/
							CreateFrame createFrame = new CreateFrame();
//							byte[] data = ghstUtil.GetByteFromString("CA3500010001004585AC53"); // CID 개통
							byte[] data = ghstUtil.GetByteFromString("CA35000100010155A4AC53"); // NCID 개통 // 프레임을 직접 만들어 주는게 좋다
							byte[] frameBuf = createFrame.CreateHeaderFrame((byte)0x05, 0x00057001, 'I', etGwNumber.getText().toString().trim() , (short)0x01 , (short)data.length );
							ArrayList<byte[]> arDataFrame = new ArrayList<byte[]>();
							arDataFrame.add( data );
							
//							Log.i("Lia", String.valueOf( sendToServer( frameBuf, arDataFrame ) ) );
							
							Thread sendT = new Thread( new SendToServerMessage( frameBuf, arDataFrame ) );
							sendT.start();
							
							/*응답메세지 확인 폴링 쓰레드*/
							Thread poolingThread = new Thread( new PollingGetMessageFromServer(handler) );
							poolingThread.start();
							
						}
					});
			openDialog.show();
			
		}
		
	}
	
	synchronized public boolean sendToServer(byte[] headFrame, ArrayList<byte[]> arDataFrame ) {
		
		IPDAPSendSocket iPDAPSendSocket = null;
		
		try {

			iPDAPSendSocket = new IPDAPSendSocket( arBeanDTMF.m2mIpdapAddr,arBeanDTMF.m2mIpdapPort ); 
			
			byte Link[]; 
			// 연결 확인 절차
			{
				Link = new byte[DefineList.LINK_SIZE];
				Link = iPDAPSendSocket.SocketReadLink(  Link.length);
			}
			
			// 헤더 프래임을 전달
			{
				iPDAPSendSocket.SocketWrite( headFrame); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
			}
			
			//데이터 프래임 전달 - 데이터양이 2개 이상일 경우는 루프를 돌린다 
			for( byte[] dataFrame : arDataFrame ){
				iPDAPSendSocket.SocketWrite( dataFrame ); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
			}
			
			
			// 받을 데이터 확인
			LinkedList<IPDAPDataInfo> list = new LinkedList<IPDAPDataInfo>();
			list = iPDAPSendSocket.readIPDAPSaveData();
			
			// 받은 데이터 표시 
			int count = 0;
			if( list == null ){
				Log.i("Lia","NO DATA");
			}
			for( IPDAPDataInfo obj : list ){
				
				Log.i("Lia",++count +" HEAD : " + ghstUtil.GetStringFromByte(obj.getHead()));
				if( obj.getDatList() != null )
				for( byte[] headData : obj.getDatList()){
					Log.i("Lia",count +" DATA : " + ghstUtil.GetStringFromByte(headData));
				} 
			}
			
			iPDAPSendSocket.close();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();

		}
		
		return iPDAPSendSocket.isClosed();
	}
	
	/**
	 * 데이터 프레임으로부터 맥 어드레스 파싱
	 * @param macPieceofStrData
	 * @return
	 */
	public String getMacFromData( String macPieceofStrData ) {
		
		String macStr = new String();
		
		String tempVal = new String();
		
		for( int i=0; i< macPieceofStrData.length(); i++ ) {
			
			if( i%3 != 2 ) {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
			
			} else {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
				
				macStr += String.format("%02x", Integer.parseInt( tempVal )  );
				tempVal = "";
			}
			
		}
		
		return macStr;
	}
	
	/**
	 * 메세지 리더 콜백을 위한 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			/* 메세지가 수신되었을 경우 */
			if( msg.what == 20 ) {	
				
				String openResult = new String();
				etInfoState.append("\n>수신된 메세지 처리.");
				
				LinkedList<IPDAPDataInfo> list = (LinkedList)msg.obj;
				
				for( IPDAPDataInfo obj : list ){
					
					/*0x0004 6004 면 전화번호 확인 결과 처리 해준다*/
					DefineHeaderFrame dhf = new DefineHeaderFrame();
					dhf.analysisProtocol( obj.getHead() );
					
					etInfoState.append("\n>>> 수신 메세지 종류 : " + String.format("%08x", dhf.getHid()) );
					
					if( dhf.getHid() == 0x00046004 ) {
						
						etInfoState.append( "\n>>00046004 - 전화번호 확인 응답 : " + etGwNumber.getText().toString()  );
						
						/*데이터 프레임 검사해서 유효 번호가 맞으면 개통 절차를 밟는다.*/
						if( obj.getDatList() != null ) {
							
							CreateFrame createFrame = new CreateFrame();
							ArrayList<byte[]> arDataFrame = new ArrayList<byte[]>();
							
								
							if( ghstUtil.GetStringFromByte( obj.getDatList().get(0) ).substring(12, 14).equals("00")  ) {	//유효한 전화번호면	
								
								etInfoState.append( "\n>>>서버에 존재하는 유효한 번호."  );
								//맥어드레스를 올려준다
								//개통 성공을 내려준다
								
								int frameNumber = 0;
								for( String mac : arMacList ) {
									
									if( !mac.substring(2).trim().equals("") ) {
										arDataFrame.add( createFrame.CreateDataFrame( (short)frameNumber++, ghstUtil.GetByteFromString( mac ) ) );
										
										Log.i("Lia-Mac", mac );
									}
									
								}
								
								byte[] headFrame = createFrame.CreateHeaderFrame((byte)0x05, 0x00046007, 'I', etGwNumber.getText().toString().trim() , (short)arDataFrame.size() , (short)0x13 );
								
								String str = new String();
								for( byte head : headFrame ) {
									str += String.format("%02x", head );
								}
								
								Log.i("Lia-Test", str ) ;
								
								Thread thSend = new Thread( new SendToServerMessage( headFrame, arDataFrame ) );
								thSend.start();
								
								etInfoState.append( "\n>개통 성공 메세지 송신." );
								
//								//시간정보 송신
//								Calendar now = Calendar.getInstance();
//								SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//								
//								byte[] byteDateTime = new byte[12];
//								int i = 0;
//								 
//								for( byte a : dateFormat.format( now.getTime() ).substring(2, 14).getBytes() ) {
//									byteDateTime[i++] = (byte) (a - 0x30);
//								}
//								
//								gmc.messageSend( (short) 12, byteDateTime );
								
								try {
									Thread.sleep(3000);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								gmc.messageSendString( "*0503010162#" );
								gmc.messageSendString( "*3#" );
								
								etInfoState.append( "\n>*개통완료." );
								
								openResult = "개통성공";
								
							} else {
								etInfoState.append( "\n>>>서버에 존재하지 않는 번호."  );
								//개통 실패를 내려준다
								
								gmc.messageSendString( "#*0503011162#" );
								gmc.messageSendString( "*3#" );
								etInfoState.append( "\n>*개통실패." );
								
								openResult = "개통실패 - 서버에 존재하지 않는 번호";
							}
							
						}
						
					}
					
				}
				
				/*파일출력*/
//				File file = Environment.getExternalStorageDirectory().getAbsoluteFile();
				
				String appAbsoluteFile = Environment.getExternalStorageDirectory().getAbsolutePath();
				File file = new File( appAbsoluteFile + "/teleField/openHistory" );
				
				//시간
				long time = System.currentTimeMillis();
				Date date = new Date(time);
				SimpleDateFormat sdg = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss a");
				
				TelephonyManager mTelephonyMgr = (TelephonyManager) GwOpen.this.getSystemService(Context.TELEPHONY_SERVICE);
				FileController fc = new FileController( file, "openHistory_" + mTelephonyMgr.getLine1Number().replace("-", "").replace("+82", "0") + ".txt" );
				
				String resultReport = new String();
				
				resultReport += etGwNumber.getText();
				resultReport += "," + etName.getText();
				resultReport += "," + etAddress.getText();
				resultReport += "," + sdg.format(date).toString();	//날짜 시간
				resultReport += "," + openResult;	//개통결과
				resultReport += "," + GwOpen.this.getIntent().getStringExtra("0x01");	//맥주소
				
				resultReport = fc.readFileByString() + resultReport;
				
				fc.writeFileByString( resultReport );
				
				Toast.makeText( GwOpen.this, "파일 출력 완료" , Toast.LENGTH_SHORT ).show();
				
				etInfoState.append("\n. 파일출력 완료.");
				
				btnGwOpen.setClickable(true);
				btnGwOpen.setFocusable(true);
				btnGwOpen.setText("개통시작");
				
				
			} else if( msg.what == 21 ) {	/*수신된 메세지가 없을 경우*/
				
				etInfoState.append("\n>수신된 메세지가 없습니다.");
				
				Toast.makeText( GwOpen.this, "서버에서 응답된 메세지가 없습니다. 다시 재시도 하십시오.", Toast.LENGTH_SHORT ).show();
				
				
				/*파일출력*/
				//File file = Environment.getExternalStorageDirectory().getAbsoluteFile();
				String appAbsoluteFile = Environment.getExternalStorageDirectory().getAbsolutePath();
				File file = new File( appAbsoluteFile + "/teleField/openHistory" );
				
				//시간
				long time = System.currentTimeMillis();
				Date date = new Date(time);
				SimpleDateFormat sdg = new SimpleDateFormat("* yyyy-MM-dd HH:mm:ss a ");
				
				TelephonyManager mTelephonyMgr = (TelephonyManager) GwOpen.this.getSystemService(Context.TELEPHONY_SERVICE);
				FileController fc = new FileController( file, "openHistory_" + mTelephonyMgr.getLine1Number().replace("-", "").replace("+82", "0") + ".txt" );
				
				String resultReport = new String();
				
				resultReport += etGwNumber.getText();
				resultReport += "," + etName.getText();
				resultReport += "," + etAddress.getText();
				resultReport += "," + sdg.format(date).toString();	//날짜 시간
				resultReport += "," + "서버에서 응답된 메세지 없음.";	//개통결과
				resultReport += "," + GwOpen.this.getIntent().getStringExtra("0x01");	//맥주소
				
				resultReport = fc.readFileByString() + resultReport;
				
				fc.writeFileByString( resultReport );
				
				Toast.makeText( GwOpen.this, "파일 출력 완료" , Toast.LENGTH_SHORT ).show();
				
				etInfoState.append("\n. 파일출력 완료.");
				
				
				btnGwOpen.setClickable(true);
				btnGwOpen.setFocusable(true);
				btnGwOpen.setText("개통시작");
				
			} else if( msg.what == 30 ) {
				etInfoState.append( "\n> *메세지 폴링 쓰레드  시작."  );
			} else if( msg.what == 31 ) {
				etInfoState.append( "\n> * 맥 보내기 쓰레드 시작."  );
			}
			
    	}
    };
    
    class PollingGetMessageFromServer implements Runnable {

    	boolean doingLoop = false;
    	
    	Handler mHandler;
    	
    	public PollingGetMessageFromServer( Handler h ) {
    		this.mHandler = h;
    	}
    	
		@Override
		public void run() {
			
			
			Message msg = new Message();
			msg.what = 30;
			handler.sendMessage(msg);
			
			LinkedList<IPDAPDataInfo> list = new LinkedList<IPDAPDataInfo>();
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
			try {
				IPDAPSendSocket iPDAPSendSocket = new IPDAPSendSocket(  arBeanDTMF.m2mIpdapAddr,arBeanDTMF.m2mIpdapPort ); 

				CreateFrame createFrame = new CreateFrame(); 
				byte[] frameBuf = createFrame.CreateHeaderFrame((byte)0x05, 0x00000000, 'I',  etGwNumber.getText().toString().trim()  , (short)0x00 , (short)0);
				
				byte Link[];
				
				// 연결 확인 절차
				{
					Link = new byte[DefineList.LINK_SIZE];
					Link = iPDAPSendSocket.SocketReadLink(  Link.length);
				}
				
				// 헤더 프래임을 전달
				{
					iPDAPSendSocket.SocketWrite( frameBuf); 
					Link = iPDAPSendSocket.SocketReadLink( Link.length);
				}
				
				// 받을 데이터 확인
				synchronized ( iPDAPSendSocket ) {
					list = iPDAPSendSocket.readIPDAPSaveData();
				}
				
				iPDAPSendSocket.close();
				
				iPDAPSendSocket = null;
				
			} catch (UnknownHostException e) {
				e.printStackTrace();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if( list.size() > 0 ) {
			
				msg = new Message();
				msg.what = 20;
				msg.obj = list;
				mHandler.sendMessage(msg);
				
			} else {
				msg = new Message();
				msg.what = 21;
				handler.sendMessage(msg);
			}
			
		}
    	
    	
    }
    
    class SendToServerMessage implements Runnable {
    	
    	byte[] headFrame;
    	ArrayList<byte[]> arDataFrame;
    	
    	public  SendToServerMessage(byte[] headFrame, ArrayList<byte[]> arDataFrame ) {
    		this.headFrame = headFrame;
    		this.arDataFrame = arDataFrame;
    	}

		@Override
		public void run() {
			
			Message msg = new Message();
			msg.what = 31;
			handler.sendMessage(msg);
			 
			try {
				IPDAPSendSocket iPDAPSendSocket = new IPDAPSendSocket( arBeanDTMF.m2mIpdapAddr,arBeanDTMF.m2mIpdapPort ); 

				byte Link[]; 
				// 연결 확인 절차
				{
					Link = new byte[DefineList.LINK_SIZE];
					Link = iPDAPSendSocket.SocketReadLink(  Link.length);
				}
				
				// 헤더 프래임을 전달
				{
					iPDAPSendSocket.SocketWrite( headFrame); 
					Link = iPDAPSendSocket.SocketReadLink( Link.length);
				}
				
				//데이터 프래임 전달 - 데이터양이 2개 이상일 경우는 루프를 돌린다 
				for( byte[] dataFrame : arDataFrame ){
					iPDAPSendSocket.SocketWrite( dataFrame ); 
					Link = iPDAPSendSocket.SocketReadLink( Link.length);
				}
				
				
				// 받을 데이터 확인
				LinkedList<IPDAPDataInfo> list = new LinkedList<IPDAPDataInfo>();
				list = iPDAPSendSocket.readIPDAPSaveData();
				
				// 받은 데이터 표시 
				int count = 0;
				Log.i("Lia","메세지 센드 후 받은 데이터 길이 : " + String.valueOf( list.size() ));

				for( IPDAPDataInfo obj : list ){
					
					Log.i("Lia",++count +" HEAD : " + ghstUtil.GetStringFromByte(obj.getHead()));
					if( obj.getDatList() != null )
					for( byte[] headData : obj.getDatList()){
						Log.i("Lia",count +" DATA : " + ghstUtil.GetStringFromByte(headData));
					} 
				}
				 
				iPDAPSendSocket.close();
				
			} catch (UnknownHostException e) {
				e.printStackTrace();
				
			} catch (IOException e) {
				e.printStackTrace();

			}
			
		}
    	
    	
    }
    
}