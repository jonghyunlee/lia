package com.topperji.ghst.lia;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import com.bmk.SendSocket.common.CreateFrame;
import com.topperji.m2m.GHSTUtil;

public class NewLiaMenu extends Activity implements OnClickListener, OnCheckedChangeListener {
	ArrayList<String> arMacList;
	GHSTUtil ghstUtil = new GHSTUtil();
	Button button;
	final String TAG = "LIA";
	final int MAC_LENGTH = 16;
	final int SEND_SUCCESS = 1;
	byte c ;
	String gatewayNumber;
	EditText gw_mac;
	EditText activity_mac_01;
	EditText activity_mac_02;
	EditText fire_mac_01;
	EditText fire_mac_02;
	EditText gas_mac_01;
	EditText gas_mac_02;
	EditText door_mac;
	EditText gw_number;
	
	RadioGroup radioGroup;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_lia_menu);
		arMacList = new ArrayList<String>();

    	button = (Button)findViewById(R.id.server_send_button);
    	button.setOnClickListener(this);
	
    	gw_number = (EditText)findViewById(R.id.gw_number);
    	gw_mac = (EditText)findViewById(R.id.gw_mac_editText);
    	activity_mac_01 = (EditText)findViewById(R.id.activity_mac_01);
    	activity_mac_02 = (EditText)findViewById(R.id.activity_mac_02);
    	fire_mac_01 = (EditText)findViewById(R.id.fire_mac_01);
    	fire_mac_02 = (EditText)findViewById(R.id.fire_mac_02);
    	gas_mac_01 = (EditText)findViewById(R.id.gas_mac_01);
    	gas_mac_02 = (EditText)findViewById(R.id.gas_mac_02);
    	door_mac = (EditText)findViewById(R.id.door_mac);
 	
    	radioGroup = (RadioGroup)findViewById(R.id.radiogroup);
    	radioGroup.setOnCheckedChangeListener(this);
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.server_send_button:
				getEditText();
				sendGatewayInfoData();
				arMacList.clear();
				Toast.makeText(this, "정상적으로 메시지를 전송하였습니다.", Toast.LENGTH_SHORT).show();
				break;
		}
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		
		switch(checkedId){
		case R.id.radio_four:
			c = 0x04;
			break;
		case R.id.radio_five:
			c = 0x05;
			break;
		}
		Log.d(TAG, "c = " + c);
	}	
	
	private void sendGatewayInfoData(){
		CreateFrame createFrame = new CreateFrame();
		ArrayList<byte[]> arDataFrame = new ArrayList<byte[]>();
		short frameNumber = 0;
		for( String mac : arMacList ) {
			
			if( !mac.substring(2).trim().equals("") ) {
				arDataFrame.add( createFrame.CreateDataFrame( (short)frameNumber++, ghstUtil.GetByteFromString( mac ) ) );
				
				Log.i("Lia-Mac", mac );
			}
			
		}
		Log.d(TAG, "gatewayNumber : " + gatewayNumber + "...gatewayNumber.length : " + gatewayNumber.length() + "...c : " + c);
		if(gatewayNumber != null && gatewayNumber.length() > 0){
			byte[] headFrame = createFrame.CreateHeaderFrame(c, 0x00046007, 'I',gatewayNumber, (short)arDataFrame.size() , (short)0x13 );
			
			String str = new String();
			for( byte head : headFrame ) {
				str += String.format("%02x", head );
			}Log.d(TAG, "해더 바이트 코드로 변환 : " + str);
			Runnable run =  new SendToServerMessage( headFrame, arDataFrame ) ;
			Thread thSend = new Thread(run);
			thSend.start();
		}
		else{
			Toast.makeText(this, "게이트웨이 전화번호를 입력 하세요.", Toast.LENGTH_SHORT).show();
		}
	}
	
	private void getEditText(){
		String get_gw_number = gw_number.getText().toString();
		String get_gw_mac = gw_mac.getText().toString();
		String get_activity_mac_01 = activity_mac_01.getText().toString();
		String get_activity_mac_02 = activity_mac_02.getText().toString();
		String get_fire_mac_01 = fire_mac_01.getText().toString();
		String get_fire_mac_02 = fire_mac_02.getText().toString();
		String get_gas_mac_01 = gas_mac_01.getText().toString();
		String get_gas_mac_02 = gas_mac_02.getText().toString();
		String get_door_mac = door_mac.getText().toString();
		
		if(get_gw_number != null){
			gatewayNumber = get_gw_number;
		}
		
		if(get_gw_mac != null && get_gw_mac.length() == MAC_LENGTH){
			arMacList.add(  "01" + get_gw_mac);	//본체
		}else{
			arMacList.add("01");
		}
		if(get_activity_mac_01 != null && get_activity_mac_01.length() == MAC_LENGTH){
			arMacList.add(  "11" + get_activity_mac_01);	//활동센서 1
		}else{
			arMacList.add("11");
		}
		if(get_activity_mac_02 != null && get_activity_mac_02.length() == MAC_LENGTH){
			arMacList.add(  "12" + get_activity_mac_02);	//활동센서 2
		}else{
			arMacList.add("12");
		}
		if(get_fire_mac_01 != null && get_fire_mac_01.length() == MAC_LENGTH){
			arMacList.add(  "21" + get_fire_mac_01);	//화재센서 1
		}else{
			arMacList.add("21");
		}
		if(get_fire_mac_02 != null && get_fire_mac_02.length() == MAC_LENGTH){
			arMacList.add(  "22" + get_fire_mac_02);	//화재센서 2
		}else{
			arMacList.add("22");
		}
		if(get_gas_mac_01 != null && get_gas_mac_01.length() == MAC_LENGTH){
			arMacList.add(  "31" + get_gas_mac_01);	//가스센서 1
		}else{
			arMacList.add("31");
		}
		if(get_gas_mac_02 != null && get_gas_mac_02.length() == MAC_LENGTH){
			arMacList.add(  "32" + get_gas_mac_02);	//가스센서 2
		}else{
			arMacList.add("32");
		}
		if(get_door_mac != null && get_door_mac.length() == MAC_LENGTH){
			arMacList.add(  "41" + get_door_mac);	//외출센서
		}else{
			arMacList.add("41");
		}
		for(String macList : arMacList){
			Log.d(TAG, macList);
		}
	}

	
	
	
}
