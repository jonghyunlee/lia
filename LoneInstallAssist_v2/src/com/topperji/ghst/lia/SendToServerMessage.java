package com.topperji.ghst.lia;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;

import android.util.Log;

import com.bmk.SendSocket.IPDAPSendSocket.IPDAPDataInfo;
import com.bmk.SendSocket.IPDAPSendSocket.IPDAPSendSocket;
import com.bmk.SendSocket.common.DefineList;
import com.topperji.m2m.GHSTUtil;

public class SendToServerMessage implements Runnable {
	final String TAG = "LIA";
	byte[] headFrame;
	final int SEND_SUCCESS = 1;
	ArrayList<byte[]> arDataFrame;
	ArBeanDTMF arBeanDTMF;
	GHSTUtil ghstUtil = new GHSTUtil();
	
	public SendToServerMessage(byte[] headFrame, ArrayList<byte[]> arDataFrame ) {
		this.headFrame = headFrame;
		this.arDataFrame = arDataFrame;
	}

	@Override
	public void run() {
		
		arBeanDTMF = ArBeanDTMF.getInstance();
		try {
			IPDAPSendSocket iPDAPSendSocket = new IPDAPSendSocket( arBeanDTMF.m2mIpdapAddr,arBeanDTMF.m2mIpdapPort ); 
			Log.d(TAG, "IP address : " + arBeanDTMF.m2mIpdapAddr + "...port : " + arBeanDTMF.m2mIpdapPort);
			Log.d(TAG, "iPDAPSendSocket is connected : " + iPDAPSendSocket.isConnected());
			byte Link[]; 
			// 연결 확인 절차
			{
				Link = new byte[DefineList.LINK_SIZE];
				Link = iPDAPSendSocket.SocketReadLink(  Link.length);
				Log.d(TAG, "연결을 확인하는 중");
			}
			
			// 헤더 프래임을 전달
			{
				iPDAPSendSocket.SocketWrite( headFrame); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
				Log.d(TAG, "헤더 프래임을 전달");
			}
			
			//데이터 프래임 전달 - 데이터양이 2개 이상일 경우는 루프를 돌린다 
			for( byte[] dataFrame : arDataFrame ){
				iPDAPSendSocket.SocketWrite( dataFrame ); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
				Log.d(TAG, "데이터 프레임 전달");
			}
			
			
			// 받을 데이터 확인
			LinkedList<IPDAPDataInfo> list = new LinkedList<IPDAPDataInfo>();
			list = iPDAPSendSocket.readIPDAPSaveData();
			
			// 받은 데이터 표시 
			int count = 0;
			Log.i("Lia","메세지 센드 후 받은 데이터 길이 : " + String.valueOf( list.size() ));
			for( IPDAPDataInfo obj : list ){
				
				Log.i("Lia",++count +" HEAD : " + ghstUtil.GetStringFromByte(obj.getHead()));
				if( obj.getDatList() != null )
				for( byte[] headData : obj.getDatList()){
					Log.i("Lia",count +" DATA : " + ghstUtil.GetStringFromByte(headData));
				} 
			}
			 
			iPDAPSendSocket.close();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();

		}
		
	}
	
	
}