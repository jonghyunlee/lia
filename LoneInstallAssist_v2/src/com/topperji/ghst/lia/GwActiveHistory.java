package com.topperji.ghst.lia;

import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.Toast;

import com.bmk.SendSocket.IPDAPSendSocket.IPDAPDataInfo;
import com.bmk.SendSocket.IPDAPSendSocket.IPDAPSendSocket;
import com.bmk.SendSocket.common.DefineList;
import com.topperji.ftdiController.FtdiController;
import com.topperji.ftdiController.GwMessageComm;
import com.topperji.ghst.liaProcess.BeanDTMF;
import com.topperji.ghst.liaProcess.ProtocolProcessor;
import com.topperji.m2m.CreateDTMFFrame;
import com.topperji.m2m.GHSTUtil;
import com.topperji.util.FileController;
import com.topperji.util.UtilsAboutString;

public class GwActiveHistory extends Activity implements OnClickListener {
	
	ImageView imgViewState;
	EditText etActiveHistory;
	EditText etInfoState;
	TabHost tabHost;
	TabHost.TabSpec spec;
	Resources res;
	
	Button btnLoadActive;
	
	CreateDTMFFrame createDtmfFrame = new CreateDTMFFrame();
	
	FtdiController ftdi;
	
	GwMessageComm gmc;
	
	ArrayList<BeanDTMF> arBeanDTMF;
	
	int flag = 0;
	
	int flagMessageTypeGet = 0; // 메세지별 받았는지 여부 확인 변수 0: 하나도 안받음, 1: 맥정보까지 받음, 2:설정정보까지 받음
	
	GHSTUtil ghstUtil = new GHSTUtil();
	
	boolean isActiveHistoryWeek = false;
	
	String resultReport = new String(); //결과 저장
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gw_active_history);
		
		ftdi = FtdiController.getInstance(this);
		
		res = getResources(); //아이콘 이미지를 위한 리소스화
		
		btnLoadActive = (Button)findViewById(R.id.btn_gw_get_active);
		btnLoadActive.setOnClickListener( this);
        
		etActiveHistory = (EditText)findViewById(R.id.et_active_history);
		etInfoState = (EditText)findViewById(R.id.et_state_active);
		
		etActiveHistory.setFocusable(false);
		etInfoState.setFocusable(false);
		
    	gmc = new GwMessageComm(this, ftdi, handler );
		
    	arBeanDTMF = new ArrayList<BeanDTMF>();
    	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.gw_info, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		
		if( v.getId() == R.id.btn_gw_get_active ) {
			
			etActiveHistory.setText("");
			btnLoadActive.setClickable(false);
			btnLoadActive.setFocusable(false);
			btnLoadActive.setText("데이터 송수신중!!");
			
			if( ftdi.isConnected() ) {
				Toast.makeText(this, "게이트웨이 정보를 가져오고 있습니다.", Toast.LENGTH_SHORT ).show();
				
				etInfoState.setText( "1. 데이터 송수신중.." );
				
				flagMessageTypeGet = 1;
				
				gmc.messageSend( (short) 16, null );
				
			} else {
			
			}
		}
	}
	
	synchronized public boolean sendToServer(byte[] headFrame, ArrayList<byte[]> arDataFrame ) {
		
		IPDAPSendSocket iPDAPSendSocket = null;
		
		try {

			iPDAPSendSocket = new IPDAPSendSocket("222.122.128.45",14500 ); 
			
			byte Link[]; 
			// 연결 확인 절차
			{
				Link = new byte[DefineList.LINK_SIZE];
				Link = iPDAPSendSocket.SocketReadLink(  Link.length);
			}
			
			// 헤더 프래임을 전달
			{
				iPDAPSendSocket.SocketWrite( headFrame); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
			}
			
			//데이터 프래임 전달 - 데이터양이 2개 이상일 경우는 루프를 돌린다 
			for( byte[] dataFrame : arDataFrame ){
				iPDAPSendSocket.SocketWrite( dataFrame ); 
				Link = iPDAPSendSocket.SocketReadLink( Link.length);
			}
			
			
			// 받을 데이터 확인
			LinkedList<IPDAPDataInfo> list = new LinkedList<IPDAPDataInfo>();
			list = iPDAPSendSocket.readIPDAPSaveData();
			
			// 받은 데이터 표시 
			int count = 0;
			if( list == null ){
				Log.i("Lia","NO DATA");
			}
			for( IPDAPDataInfo obj : list ){
				
				Log.i("Lia",++count +" HEAD : " + ghstUtil.GetStringFromByte(obj.getHead()));
				if( obj.getDatList() != null )
				for( byte[] headData : obj.getDatList()){
					Log.i("Lia",count +" DATA : " + ghstUtil.GetStringFromByte(headData));
				} 
			}
			
			iPDAPSendSocket.close();
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();

		}
		
		return iPDAPSendSocket.isClosed();
	}
	
	/**
	 * 데이터 프레임으로부터 맥 어드레스 파싱
	 * @param macPieceofStrData
	 * @return
	 */
	public String getMacFromData( String macPieceofStrData ) {
		
		String macStr = new String();
		
		String tempVal = new String();
		
		for( int i=0; i< macPieceofStrData.length(); i++ ) {
			
			if( i%3 != 2 ) {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
			
			} else {
				tempVal += macPieceofStrData.substring(i, i+1 ) ;
				
				macStr += String.format("%02x", Integer.parseInt( tempVal )  );
				tempVal = "";
			}
			
		}
		
		return macStr;
	}
	
	int activeReleaseTerm = 0;
	int activeSensorAmount = 0;
	String activeLastDataTime = new String();
	
	public void processGetData( ) {
		
		Iterator<BeanDTMF> it = arBeanDTMF.iterator();
		
		String activeData = new String();
		
		while( it.hasNext() ) {
			
			BeanDTMF beans = it.next();
			
			if( beans.strHID.equals( "17" ) ) {	// 센서 정보일 경우
				isActiveHistoryWeek = true;
				
				activeReleaseTerm = Integer.parseInt( beans.strData.substring(0, 3) );
				activeSensorAmount = Integer.parseInt( beans.strData.substring(3, 5) );
				activeLastDataTime = "20" + beans.strData.substring(5, 15) + "00";
				
				Log.i("Lia", "헤드 프레임 구조 : " + UtilsAboutString.getDateTimeByMinus( activeLastDataTime, 25) );
				
			} else if( beans.strHID.equals( "18" ) ) {
				
				activeData += beans.strData;
				
			}
			
		}
		
//		Log.i("Lia", "활동 데이터 : " + activeData );
//		Log.i("Lia", "총계 : " + String.valueOf( activeData.length() / 2  ) );
		
		int dataHourAmount = activeData.length()/2 / activeSensorAmount; //데이터의 갯수는 문자 2개씩 센서 갯수만큼 나눠준 값.
		
		resultReport += "기록된 가장 최근 데이터 : " 
						+ activeLastDataTime.substring(0, 4) + "년 " 
						+ activeLastDataTime.substring(4, 6) + "월 "
						+ activeLastDataTime.substring(6, 8) + "일 "
						+ activeLastDataTime.substring(8, 10) + "시 "
						+ activeLastDataTime.substring(10, 12) + "분 "+ "\r\n\r\n";
		
		for( int i=0; i<dataHourAmount; i++ ) {
			
			String dataTime = UtilsAboutString.getDateTimeByMinus( activeLastDataTime, (dataHourAmount-i-1) );
			
			resultReport += String.format("%03d", i+1 ) + ". "
							+ dataTime.substring(0, 4) + "년 "
							+ dataTime.substring(4, 6) + "월 "
							+ dataTime.substring(6, 8) + "일 "
							+ dataTime.substring(8, 10) + "시   ▶"
//							+ dataTime.substring(10, 12) + "분   ▶ "
							+ "센서1 : " + activeData.substring( (2*activeSensorAmount)*i , (2*activeSensorAmount)*i + 2 )
							+ " / 센서2 : " + activeData.substring( (2*activeSensorAmount)*i + 2 , (2*activeSensorAmount)*i + 4 )
							+ "\n";
			
		}
		
//		Log.i("Lia", "결과 리포트 : " + resultReport );
			
//		arBeanDTMF.clear();
		
	}
	
	/**
	 * 메세지 리더 콜백을 위한 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == 10 ) {
				
				etInfoState.append("\n2. GW로부터 데이터 수신 완료.");
				
				ArrayList<String> getDataList = (ArrayList<String>)msg.obj;
				
				//배열 데이터 처리
				StringBuffer str = new StringBuffer();
				
				Iterator<String> it = getDataList.iterator();
				
				while( it.hasNext() ) {
					
					String temp = it.next();
					
					if( ProtocolProcessor.checkDTMFFrame(temp ) ) {
						
						str.append( temp + "\n" );
						arBeanDTMF.add( ProtocolProcessor.getBeanFromDTMFFrame( temp ) );
						
//						processGetData( ProtocolProcessor.getBeanFromDTMFFrame( temp ) );
						
					} else {
						etInfoState.append("\n** 데이터 수신 에러.");
						
					}
					
				}
				
				etInfoState.append("\n3. 수신 데이터 분석 및 처리.");
				
				processGetData();	//데이터 처리
				
				etActiveHistory.setText( resultReport );
				
				etInfoState.append("\n4. 처리 완료.");
				
				
				/*파일출력*/
//				File file = Environment.getExternalStorageDirectory().getAbsoluteFile();
				
				String appAbsoluteFile = Environment.getExternalStorageDirectory().getAbsolutePath();
				File file = new File( appAbsoluteFile + "/teleField/activeHistory" );
				
				//시간
				long time = System.currentTimeMillis();
				Date date = new Date(time);
				SimpleDateFormat sdg = new SimpleDateFormat("yyyyMMdd_HHmmss");
				
				FileController fc = new FileController( file, "activeHistory_" + sdg.format(date) + ".txt" );
				fc.writeFileByte( resultReport.getBytes() );
				
				Toast.makeText( GwActiveHistory.this, "파일 출력 완료" , Toast.LENGTH_SHORT ).show();
				
				etInfoState.append("\n4. 파일출력 완료.");
				
				btnLoadActive.setClickable(true);
				btnLoadActive.setFocusable(true);
				btnLoadActive.setText("데이터 가져오기");
				
//				Toast.makeText( context, receiveMessage , Toast.LENGTH_SHORT ).show();
			
			} else if( msg.what == 11 ) {	//메세지 수신중
				etInfoState.append("▼");
			}
			
			
			
    	}
    };
    
}