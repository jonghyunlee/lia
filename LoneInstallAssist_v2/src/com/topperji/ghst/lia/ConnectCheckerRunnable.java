package com.topperji.ghst.lia;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.topperji.ftdiController.FtdiController;

public class ConnectCheckerRunnable extends Thread {
	
		
	FtdiController ftdi;
	Context context;
	Handler handler;
	
	boolean threadGoing = false;
	
	boolean pauseVal = false;

	public ConnectCheckerRunnable( FtdiController ftdi, Context context, Handler handler) {
		
		this.ftdi = ftdi;
		this.context = context;
		this.handler = handler;
		
	}
	
	@Override
	public void run() {
		
		threadGoing = true;
		
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		while( threadGoing ) {
			
			/*Pause 를 적용하기 위한 루프*/
			while( pauseVal ) {
				
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
			
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if( !ftdi.isConnected() ) {
				Message msg = handler.obtainMessage();
				msg.what = 1;
				handler.sendMessage(msg);
				
				pauseRun();
			}
			
		}
	}
		
	
	public void pauseRun() {
		pauseVal = true;
	}
	
	public void resumeRun() {
		pauseVal = false;
	}
	
	public void closeRun() {
		pauseVal = false;
		threadGoing = false;
	}


}


