package com.topperji.ghst.liaProcess;

import com.topperji.m2m.CRC8;
import com.topperji.m2m.CreateDTMFFrame;

import android.util.Log;

public class ProtocolProcessor {
	
	static final char FS = '*';
	static final char FE ='#';
	
	public static final char SYSTEM_ACK = '0';
	public static final char SYSTEM_NAK = '1';
	public static final char SYSTEM_START = '2';
	public static final char SYSTEM_DATA_END = '3';
	public static final char SYSTEM_LINK_END = '4';
	
	/*프레임 데이터*/
	public void callDTMFProtocol( String dtmfFrame ) {
		
		int frameLength = Integer.parseInt( dtmfFrame.substring(0, 2) );	//길이
		Log.i("Lia", String.valueOf( frameLength) );
		
	}
	
	public static BeanDTMF getBeanFromDTMFFrame( String message ) {
		
		BeanDTMF beans = new BeanDTMF();
		
		int count = 0;
		beans.strFS = message.substring(count , ++count);		
		beans.strLeng = message.substring(count , count+=2 );
		beans.length = (short)Integer.parseInt( beans.strLeng);
		beans.strHID = message.substring(count, count+=2);
		beans.strData = message.substring(count, count+=( beans.length-2));		
		beans.strCRC = message.substring(count , count+=3);
		beans.crc = (byte)Integer.parseInt( beans.strCRC);
		beans.strFE = message.substring(count , ++count);
		
		return beans;
	}
	
	
	public static boolean checkDTMFFrame( String message ){
		
		int count = 0;
		String strFS = message.substring(count , ++count);		
		String strLeng = message.substring(count , count+=2 );
		short length = (short)Integer.parseInt(strLeng);
		String strHID = message.substring(count, count+=2);
		String strData = message.substring(count, count+=( length-2));	
		String strCRC = message.substring(count , count+=3);
		byte crc = (byte)Integer.parseInt(strCRC);
		String strFE = message.substring(count , ++count);
		
		if( strFS.getBytes()[0] != FS){
			// FS 에러 
			Log.i("Lia","FS ERROR");
			return false;
		}
		if( strFE.getBytes()[0] != FE){
			// FE 에러
			Log.i("Lia","FE ERROR");
			return false;
		}
		
		byte[] checkFrame = new byte[message.length()-4];
		String strCheck = message.substring(1,message.length()-4 );
		byte[] temp = strCheck.getBytes();
		//System.out.println(strCheck);
		for( int i = 0 ; i < temp.length ; i++ ){
			checkFrame[i] =(byte)(Integer.parseInt(String.format("%c",temp[i]))); 
		} 
		checkFrame[checkFrame.length-1] = crc;  
		if( CRC8.crc_8(checkFrame) != 0){
			// CRC ERROR
			Log.i("Lia","CRC ERROR");
			return false;
		}
		
		return true;
	}

}
