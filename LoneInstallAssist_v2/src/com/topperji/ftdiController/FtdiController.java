package com.topperji.ftdiController;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.ftdi.j2xx.D2xxManager;
import com.ftdi.j2xx.D2xxManager.D2xxException;
import com.ftdi.j2xx.FT_Device;

/**
 * Ftdi 모듈로 시리얼포트를 제어하는 모듈, 싱글톤으로 이루어져 있어 getInstance()로 객체를 생성한다.
 * @author Topper
 *
 */
public class FtdiController {
	
	static FtdiController instance;
	Context context;
	
	public static D2xxManager ftd2xx = null;
	public FT_Device ftDev = null;
	int devCount;
	
	public static final int MAX_READ_LENGTH = 1024;
	int iavailable = 0; 	//버퍼 Queue
	char[] readDataToText = new char[1024];	//read 버퍼
	byte[] readData = new byte[1024];
	StringBuffer readText = new StringBuffer();
	
	Thread readThread;	// 리더 쓰레드
	
	boolean bReadThreadGoing = false;
	
	public static FtdiController getInstance(Context context) {
		
		if( instance == null ) {
			instance = new FtdiController(context);
		}
		
		return instance;
	}
	
	public FtdiController( Context context ) {
		this.context = context;
		
		
	}
	
	/**
	 * 현재 연결되어있는 장비의 갯수를 가져온다.
	 */
	public int getDeviceList() {
		
		try {
			ftd2xx = D2xxManager.getInstance(context);
			devCount = ftd2xx.createDeviceInfoList(context);
			
		} catch (D2xxException e) {
			e.printStackTrace();
		}
		
		return devCount;
	}
	
	/**
	 * getDeviceList를 통해 얻은 장비 갯수에서 얻은 장비 index를 기준으로 장비 정보를 설정한다.
	 * index 는 devCount 보다 작아야만 한다. 
	 * @param index
	 */
	public void setDeviceInfo( int index, byte bitMode, int baudRate, byte dataCharacteristics, short flowControl ) {
		
		if( index < devCount ) {
			
			try {
				ftDev = ftd2xx.openByIndex( context, index);
				
				if( ftDev.isOpen() ) {
					ftDev.setBitMode( (byte)0, bitMode );
					ftDev.setBaudRate( baudRate );
					ftDev.setDataCharacteristics( dataCharacteristics, D2xxManager.FT_STOP_BITS_1, D2xxManager.FT_PARITY_NONE );
					ftDev.setFlowControl( flowControl, (byte)0x0b, (byte)0x0d );
					
					/*리더 및 커넥션 체크 쓰레드 생성*/
					if(false == bReadThreadGoing) {
						
						bReadThreadGoing = true;
						
						readThread = new ReadThread(handler);
						readThread.start();
						
						Thread conCheckThread = new Thread( new CheckConnection(handler) );
						conCheckThread.start();
						
					}
				}
				
				Toast.makeText(context, "장비 통신 값 설정완료. ", Toast.LENGTH_SHORT ).show();
				
			} catch ( Exception e ) {
				Toast.makeText(context, "장비 통신 값 설정 실패!!! ", Toast.LENGTH_SHORT ).show();
				e.printStackTrace();
				ftDev.close();
			}
			
			
		} else {
			Toast.makeText(context, "연결된 장비 갯수보다 index 값이 큽니다. ", Toast.LENGTH_SHORT ).show();
		}
		
	}
	
	/**
	 * 강제 연결 종료
	 */
	public void closeDevice() {
		bReadThreadGoing = false;
		ftDev.close();
	}
	
	/**
	 * 연결 상태값 반환
	 * @return
	 */
	public boolean isConnected() {
		return bReadThreadGoing;
	}

	/**
	 * 연결된 장비로 메세지 보내기
	 * @param message
	 */
	public void sendMessage( String message ) {
		
		if (ftDev.isOpen() == false) {
			Log.e("j2xx", "SendMessage: device not open");
		}

		ftDev.setLatencyTimer((byte) 16);

		byte[] OutData = message.getBytes();
		
//		Toast.makeText(context, message, Toast.LENGTH_SHORT ).show();
		
		ftDev.write(OutData, message.length() );
	}
	
	/**
	 * 현재 버퍼 비움. 버퍼 리턴된 값을 다 읽고 나면 버퍼를 비워야 함.
	 */
	public void readBufferClear() {
		readText.delete(0, readText.length() );
	}
	
	/**
	 * 현재 버퍼에 리턴된 값 반환
	 * @return
	 */
	public StringBuffer readBufferGet() {
		
		return readText;
	}
	
	/**
	 * 쓰레드 메세지 처리 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == 2 ) {
				if(iavailable > 0) {
	//    			((MainActivity)context).tvRead.append(String.copyValueOf(readDataToText, 0, iavailable));
	    			readText.append(String.copyValueOf(readDataToText, 0, iavailable));
	    		} 
//				Toast.makeText(context, readText, Toast.LENGTH_SHORT ).show();
			}
			
			if( msg.what == 1 ) {
				Toast.makeText( context, "연결된 장비가 끊어졌습니다. ", Toast.LENGTH_SHORT ).show();
				bReadThreadGoing = false;
			}
			
    	}
    };

    /**
     * TX 버퍼 리더 쓰레드
     * @author Topper
     *
     */
	private class ReadThread  extends Thread {
		Handler mHandler;

		public ReadThread(Handler h){
			mHandler = h;
			this.setPriority(Thread.MIN_PRIORITY);
		}

		@Override
		public void run() {
			int i;

			while(true == bReadThreadGoing) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				}

				synchronized(ftDev) {
					iavailable = ftDev.getQueueStatus();				
					if (iavailable > 0) {
						
						if(iavailable > MAX_READ_LENGTH){
							iavailable = MAX_READ_LENGTH;
						}
						
						ftDev.read(readData, iavailable);
						for (i = 0; i < iavailable; i++) {
							readDataToText[i] = (char) readData[i];
						}
						Message msg = mHandler.obtainMessage();
						msg.what = 2;
						mHandler.sendMessage(msg);
					}
				}
			}
		}

	}
	
	/**
	 * 장비 커넥션 상태 체크 쓰레드
	 * @author Topper
	 *
	 */
	class CheckConnection implements Runnable {
		
		Handler mHandler;
		
		public CheckConnection(Handler handler) {
			this.mHandler = handler;
		}

		@Override
		public void run() {
			
			while( bReadThreadGoing ) {
				
				if( getDeviceList() == 0 ) {
					Message msg = mHandler.obtainMessage();
					msg.what = 1;
					mHandler.sendMessage(msg);
					
				}
				
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}
		
	}	

}
