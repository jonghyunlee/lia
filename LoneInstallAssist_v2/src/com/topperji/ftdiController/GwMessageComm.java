package com.topperji.ftdiController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.topperji.m2m.CreateDTMFFrame;

public class GwMessageComm {
	
	final int FLAG_TRY_COUNT = 20;
	
	final String MSG_ACK = "*0#";
	final String MSG_NAK = "*1#";
	final String MSG_START = "*2#";
	final String MSG_END = "*3#";
	
	static CreateDTMFFrame createDtmfFrame = new CreateDTMFFrame();
	
	FtdiController ftdi;
	Context context;
	Handler handlerCallBack;
	
	ArrayList<String> getDataList;
	ArrayList<String> getDataListNoHead;
	
	public GwMessageComm( Context context, FtdiController ftdi, Handler handler ) {
		
		this.ftdi =ftdi;
		this.context = context;
		this.handlerCallBack = handler;
		
		getDataList = new ArrayList<String>();
		getDataListNoHead = new ArrayList<String>();
	}
	
	public void messageSend( short hid, byte[] data ) {
		
		if( ftdi.isConnected() ) {
			Thread recvTh = new Thread( new MessageRequestToGw( handler , CreateDTMFFrame.CreateDTMF( hid, data) ) );
			recvTh.start();
			
		} else {
			Toast.makeText(context, "SendMessage( : )연결이 안되어있습니다.", Toast.LENGTH_SHORT ).show();
		}
		
		
	}
	
	/**
	 * 게이트웨이로 부터 받은 메세지 리턴.
	 * @return
	 */
	public ArrayList<String> getMessageFromGw() {
		
		ArrayList<String> returnArr = (ArrayList<String>) getDataList.clone();
		getDataList.clear();
		
		return returnArr;
	}
	
	/**
	 * 종료 메세지 전송 - 게이트웨이 시리얼 모드가 종료됨.
	 */
	public void messageSendEnd() {
		
		ftdi.sendMessage( MSG_END );
		
	}
	
	/**
	 * 메세지를 직접 전송
	 */
	public void messageSendString( String messageToneCharString) {
		
		ftdi.sendMessage( messageToneCharString );
		
	}
	
	/**
	 * 쓰레드 메세지 처리 핸들러
	 */
	final Handler handler =  new Handler() {
    	
		@Override
    	public void handleMessage(Message msg) {
    		
			if( msg.what == 4 ) {
				String receiveMessage = (String)msg.obj;
				
				getDataList.add( receiveMessage );
				
				/*메세지 수신중임을 호출*/
				Message msgCallBack = new Message();
				msgCallBack.what = 11;
				handlerCallBack.sendMessage( msgCallBack );
				
//				Toast.makeText( context, receiveMessage , Toast.LENGTH_SHORT ).show();
				
			} else if( msg.what == 5 ) {	//수신 완료
				
				Message msgCallBack = new Message();
				msgCallBack.obj = getDataList.clone();
				msgCallBack.what = 10;
				handlerCallBack.sendMessage( msgCallBack );
				
				if( getDataListNoHead.size() > 0  ) {	//헤드없는 데이터가 1개이상 쌓이면
					
					msgCallBack = new Message();
					msgCallBack.obj = getDataListNoHead.clone();
					msgCallBack.what = 11;
					handlerCallBack.sendMessage( msgCallBack );
					
				}
				
				getDataList.clear();
				getDataListNoHead.clear();
				
			} else if( msg.what == 6 ) {	//헤드 갖추지 못한 수신 완료
				String receiveMessage = (String)msg.obj;
				
				getDataListNoHead.add( receiveMessage );
				
				/*메세지 수신중임을 호출*/
				Message msgCallBack = new Message();
				msgCallBack.what = 11;
				handlerCallBack.sendMessage( msgCallBack );
				
			}
			
    	}
    };
	
    class MessageRequestToGw implements Runnable {
    	
    	Handler mHandler;
    	String sendMessage;
    	String receiveMessage;
    	
    	boolean enableLoop = true;
    	
    	boolean messageSent = false;
    	
    	public MessageRequestToGw( Handler h , String sendMessage ) {
    		Log.i("Lia", "게이트웨이로 보내는 거 : " + sendMessage );
    		mHandler = h;
    		this.sendMessage = sendMessage;
    	}

		@Override
		public void run() {
			
			enableLoop = true;
			
			int flag = FLAG_TRY_COUNT;
			
			String buffer = new String();	//받은 메세지
			ftdi.readBufferClear();
			
			ftdi.sendMessage( MSG_START );	//최초 메세지 보낼게 있다고 게이트웨이에 보냄
			
			Log.i("Lia", "서버에 보낸 : " + MSG_START );
			
			while( enableLoop && flag > 0 ) {
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				String tempRead = ftdi.readBufferGet().toString();
				
				if( tempRead.contains("*") && tempRead.contains("#") ) {	//메세지를 FS, FE까지 받았을 때
					
					Log.i("Lia", "서버에서 받은 메세지 : " + tempRead );
					
					if( tempRead.contains( sendMessage ) ) {	//센드 메세지가 포함된 경우 - 에코로 포함시켜서 돌아온 경우
						tempRead =  tempRead.replace( sendMessage, "" );
						Log.i("Lia", "센드 메세지 : " + sendMessage );
						
					} else if(  tempRead.contains( MSG_START ) ) {	//스타트 메세지가 포함된 경우 - 에코로 포함시켜서 돌아온 경우
						tempRead =  tempRead.replace( MSG_START, "" );
						Log.i("Lia", "센드 메세지 : " + MSG_START );
						
					}
					
					
					
					if( tempRead.equals( MSG_END ) ) {	//*3#이면 종료
						
						ftdi.readBufferClear();
						
						enableLoop = false;
						
						//시간정보 송신
//						Calendar now = Calendar.getInstance();
//						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
//						
//						byte[] byteDateTime = new byte[12];
//						int i = 0;
//						 
//						for( byte a : dateFormat.format( now.getTime() ).substring(2, 14).getBytes() ) {
//							byteDateTime[i++] = (byte) (a - 0x30);
//						}
//						
//						messageSend( (short) 12, byteDateTime );
//						ftdi.sendMessage( sendMessage );
						
						Log.i("Lia", "*3#이면 종료인데 보내는 메세지 : " + sendMessage);
					
					} else if( tempRead.equals( MSG_ACK ) ) {	//*0# 이면 다음 입력 대기
						
						ftdi.readBufferClear();
						flag = FLAG_TRY_COUNT;
						
						/*최초로 게이트웨이에서 응답이 날라온 시점에서 보내고자 하는 메세지를 보낸다.*/
						if( !messageSent ) {
							ftdi.sendMessage( sendMessage );
							messageSent = true;
						}
						
						
					} else {	//나머지는 메세지 받아 ArrayList에 저장
						
//						Log.i("LIA", CreateDTMFFrame.CheckDTMFFrame( tempRead ) );
//						Log.i("Lia", "서버에서 받은 메세지 : " + tempRead );

						Message msg = mHandler.obtainMessage();
						
						buffer = tempRead;
						ftdi.readBufferClear();
						
						flag = FLAG_TRY_COUNT;
						
						msg.what = 4;	//유효 데이터 리드 완료 
						msg.obj = buffer;
						mHandler.sendMessage(msg);
						
						ftdi.sendMessage( MSG_ACK );	//받았다고 응답 보내야함.
					}
							
				} 
				
//				else if( tempRead.contains("#") ) {
//					
//					Message msg = mHandler.obtainMessage();
//					
//					buffer = tempRead;
//					ftdi.readBufferClear();
//					
//					flag = FLAG_TRY_COUNT;
//					
//					msg.what = 6;	//유효 데이터 리드 완료 
//					msg.obj = buffer;
//					mHandler.sendMessage(msg);
//					
//					ftdi.sendMessage( MSG_ACK );	//받았다고 응답 보내야함.
//					
//				}
				
				flag--;
				
			}
			
			Message msg = mHandler.obtainMessage();
			
			msg.what = 5;	//게이트웨이에서 데이터 전송 완료
			msg.obj = buffer;
			mHandler.sendMessage(msg);
			ftdi.readBufferClear();
			
		}
		
		public void closeRun() {
			enableLoop = false;
		}
		
    }

}
