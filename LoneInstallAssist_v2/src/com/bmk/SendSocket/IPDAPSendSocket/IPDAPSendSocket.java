package com.bmk.SendSocket.IPDAPSendSocket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.LinkedList;

import com.bmk.SendSocket.IMPSendSocket;
import com.bmk.SendSocket.common.CRC16;
import com.bmk.SendSocket.common.CreateFrame;
import com.bmk.SendSocket.common.DefineHeader;
import com.bmk.SendSocket.common.DefineList;

public class IPDAPSendSocket extends Socket implements IMPSendSocket {
	public DataOutputStream out = null;
	public DataInputStream in = null;
	
	public IPDAPSendSocket(String ip , int port) throws UnknownHostException, IOException {
		super(ip , port);
		
		out = new DataOutputStream ( this.getOutputStream());
		in = new DataInputStream ( this.getInputStream());
	}
	
	
	public LinkedList<IPDAPDataInfo> readIPDAPSaveData() throws IOException{
		byte[] link;
		short linkMessage;
		CreateFrame createFrame = new CreateFrame();
		link = createFrame.CreateLinkFrame(CreateFrame.LINK_IPPDA_DATA_NONE);
		
		LinkedList<IPDAPDataInfo> IPDAPDataList = new LinkedList<IPDAPDataInfo>();
		while( true ){
			link = SocketReadLink(link.length); 
			if( link == null ){
				break;
			}
			linkMessage = (short) (((link[1]<<8)&0xFF00) | (link[2]&0xFF)); 
			if( linkMessage == CreateFrame.LINK_IPPDA_DATA_NEWHEAD) { 
				byte headerFrame[] = null;
				if( ( headerFrame = SocketRead(CreateFrame.HEADER_SIZE) )==null ){ 	
					return null;
				}
				
				//Head recv
				DefineHeader head = new DefineHeader();
				if (  head.setHeader(headerFrame) != 0 ){
					return null;
				} 
				// recv DataFrame
				LinkedList<byte[]> dataList;
				dataList=SocketReadData(  head.MN , head.NBS ); 
				
				// list add
				IPDAPDataList.add( new IPDAPDataInfo(headerFrame, dataList));
			}else {
				break;
			}
			
		}
		return IPDAPDataList;
			
			
	}
	
	@Override
	public STATE SocketWrite( byte[] writeBuf)throws IOException{
			
			out.write(writeBuf , 0 , writeBuf.length);
			out.flush();
		
		return STATE.SERVER_OK;
	}
	
	@Override
	public LinkedList<byte[]> SocketReadData(  short maxCount , short frameSize)throws IOException{
		if( maxCount == 0 )
			return null;
		if ( frameSize == 0 ) 
			return null; 
		LinkedList <byte[]> list = new LinkedList<byte[]>();
		byte[] frame = null; 
		for( short i = 0 ;  i< maxCount ; i++ ){
			if( (frame=SocketRead( frameSize )) == null){
				return null;
			}
			list.addLast(frame);
		} 
		
		
		return list;
	}
	
	@Override
	public byte[] SocketReadLink( int readSize ) throws IOException{
		byte[] link = null;
		link = new byte[readSize];
		if ( in.read(link, 0, readSize) < 0) {
			return null;
		}
		return link;
	}
	
	@Override
	public byte[] SocketRead(int readSize)throws IOException{
		byte[] frame = null;
		byte[] link = null;
		CreateFrame createFrame = new CreateFrame();
 
			
			
			for( int i = 0 ; i < 5 ;i++ ){
				STATE reVal;
				frame = new byte[readSize];
				if( i == 4 ){
					link = createFrame.CreateLinkFrame(CreateFrame.LINK_NAK);
					SocketWrite( link);
					return null;
				}
				if( in.read(frame, 0, readSize) <= 0 ){ 
					return null;
				}
				if( CheckCRC( frame , i+1 ) != STATE.SERVER_OK ) {
					link = createFrame.CreateLinkFrame(CreateFrame.LINK_NAK_CRC);
					SocketWrite(link);
				}
				else if( (reVal= CheckFrame( frame , i+1)) != STATE.SERVER_OK ){
					if( reVal == STATE.ERROR_RECVDATA_FS ){
						link = createFrame.CreateLinkFrame(CreateFrame.LINK_NAK_FS);
						SocketWrite( link);
					}else if( reVal == STATE.ERROR_RECVDATA_FE ){
						link = createFrame.CreateLinkFrame(CreateFrame.LINK_NAK_FE);
						SocketWrite( link);
					}
				}else {
					link = createFrame.CreateLinkFrame(CreateFrame.LINK_ACK);
					SocketWrite(link);
					break;
				}
			}
			
	 
		return frame;
	} 
	
	@Override
	public STATE CheckCRC( byte[] buf, int reCount){
		if( buf == null ){
			return STATE.ERROR_RECVDATA_NULL;
		}
		if( CRC16.updateH_crc((short)0, buf, (short)(buf.length-2)) != 0 ){  
			return STATE.ERROR_RECVDATA_CRC;
		} 
		return STATE.SERVER_OK;
	}
	
	@Override
	public STATE CheckFrame( byte[] buf , int reCount){
		if( (buf[0] != DefineList.FS[0]) ||  (buf[1] != DefineList.FS[1])){ 	 
			return STATE.ERROR_RECVDATA_FS;
		}
		if( (buf[buf.length-2] != DefineList.FE[0]) ||  (buf[buf.length-1] != DefineList.FE[1])){  
			return STATE.ERROR_RECVDATA_FE;
		} 
		return STATE.SERVER_OK;
	}
	
	@Override
	public void close() throws IOException {
		in.close();
		out.close();
		super.close();
		
		in = null;
		out = null;
		
	}
	
	public boolean isClosed() {
		return super.isClosed();
	}

}
