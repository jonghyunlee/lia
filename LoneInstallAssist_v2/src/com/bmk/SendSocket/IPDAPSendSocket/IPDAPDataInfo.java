package com.bmk.SendSocket.IPDAPSendSocket;

import java.util.LinkedList;

import com.bmk.SendSocket.common.DefineHeader;

public class IPDAPDataInfo {
	byte[] head;
	LinkedList<byte[]> dataList;
	public IPDAPDataInfo(byte[] head, LinkedList<byte[]> dataList ){
		this.head = head;
		this.dataList = dataList;
	}
	
	public byte[] getHead(){
		return head;
	}
	
	public LinkedList<byte[]> getDatList(){
		return dataList;
	}
	
}
