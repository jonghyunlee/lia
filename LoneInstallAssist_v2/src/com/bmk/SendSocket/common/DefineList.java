package com.bmk.SendSocket.common;



/**
 * final ���� �κ�
 * @author BMK-GHST
 *
 */
public class DefineList {
	
	public static final String CID_DAP_CONF_FILE_NAME = "conf//confCIDDAP.xml";
	public static final String IP_DAP_CONF_FILE_NAME = "conf//confIPDAP.xml";
	public static final String DCAP4_CONF_FILE_NAME = "conf//confDCAP4.xml";
	public static final String DTMFDAP_CONF_FILE_NAME = "conf//confDTMFDAP.xml";
	public static final String DTMFDAP1_CONF_FILE_NAME = "conf//confDTMFDAP_1.xml";
	public static final String CALL_CONF_FILE_NAME = "conf//confCALL.xml";
	
	
	public static final String DTMF_CALL_NUMBER = "07012345678";
	
	public static byte[] FS = {(byte)0xCA , (byte)0x35};
	public static byte[] FE = {(byte)0xAC , (byte)0x53};
	public static byte LINK_FS = (byte)0xCA;
	public static byte LINK_FE = (byte)0x35;
	
	public static int HEADER_SIZE = 36;
	public static int LINK_SIZE = 4;
	
	public static final short LINK_ACK = 0x0001;
	public static final short LINK_NAK = 0x0010;
	public static final short LINK_NAK_CRC = 0x0011;
	public static final short LINK_NAK_FS = 0x0012;
	public static final short LINK_NAK_FE = 0x0013;
	//IPDAP 
	public static final short LINK_IPPDA_DATA_NONE = 0x0100;
	public static final short LINK_IPPDA_DATA_END = 0x0101;
	public static final short LINK_IPPDA_DATA_NEWHEAD = 0x0110;
	
	public static final int HID_4_3005 = 0x00043005;
	public static final int HID_4_3006 = 0x00043006;
	public static final int HID_4_3008 = 0x00043008;
	public static final int HID_4_3023 = 0x00043023;
	public static final int HID_4_3024 = 0x00043024;
	public static final int HID_4_3033 = 0x00043033;
	public static final int HID_4_3034 = 0x00043034;
	public static final int HID_4_3043 = 0x00043043;
	public static final int HID_4_3044 = 0x00043044;
	public static final int HID_4_3049 = 0x00043049;
	public static final int HID_4_304A = 0x0004304A;
	public static final int HID_4_3053 = 0x00043053;
	public static final int HID_4_3054 = 0x00043054;
	public static final int HID_4_3059 = 0x00043059;
	public static final int HID_4_305A = 0x0004305A;
	public static final int HID_4_3074 = 0x00043074;
	public static final int HID_4_3083 = 0x00043083;
	public static final int HID_4_3093 = 0x00043094;
	public static final int HID_4_3183 = 0x00043183;
	public static final int HID_4_3184 = 0x00043184;
	public static final int HID_4_31E3 = 0x000431E3;
	public static final int HID_4_31E4 = 0x000431E4;
	public static final int HID_4_31F3 = 0x000431F3;
	public static final int HID_4_31F4 = 0x000431F4;
	public static final int HID_4_3203 = 0x00043203;
	public static final int HID_4_3204 = 0x00043204;
	public static final int HID_4_3213 = 0x00043213;
	public static final int HID_4_3214 = 0x00043214;
	
	public static final int HID_4_4001 = 0x00044001;
	
	public static final int HID_4_5102 = 0x00045102;
	public static final int HID_4_5103 = 0x00045103;
	public static final int HID_4_5126 = 0x00045126;
	
	public static final int HID_4_6004 = 0x00046004;
	public static final int HID_4_6007 = 0x00046007;
	public static final int HID_4_600A = 0x0004600A;
	public static final int HID_4_6022 = 0x00046022;
	public static final int HID_4_6025 = 0x00046025;
	public static final int HID_4_6032 = 0x00046032;
	public static final int HID_4_6035 = 0x00046035;
	public static final int HID_4_6042 = 0x00046042;
	public static final int HID_4_6045 = 0x00046045;
	public static final int HID_4_6048 = 0x00046048;
	public static final int HID_4_604B = 0x0004604B;
	public static final int HID_4_6052 = 0x00046052;
	public static final int HID_4_6055 = 0x00046055;
	public static final int HID_4_6058 = 0x00046058;
	public static final int HID_4_605B = 0x0004605B;
	public static final int HID_4_6082 = 0x00046082;
	public static final int HID_4_6092 = 0x00046092;
	public static final int HID_4_6095 = 0x00046095;
	public static final int HID_4_6182 = 0x00046182;
	public static final int HID_4_6185 = 0x00046185;
	public static final int HID_4_61E2 = 0x000461E2;
	public static final int HID_4_61E5 = 0x000461E5;
	public static final int HID_4_61F2 = 0x000461F2;
	public static final int HID_4_61F5 = 0x000461F5;
	public static final int HID_4_6202 = 0x00046202;
	public static final int HID_4_6205 = 0x00046205;
	public static final int HID_4_6215 = 0x00046212;
	
	
}
