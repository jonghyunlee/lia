package com.bmk.SendSocket.common;


import java.util.Calendar;

  
/**
 * IMP 프레임을 생성하는 클래스
 * @author BMK-GHST
 *
 */
public class CreateFrame extends DefineList{
	/**
	 * Link 프레임을 생성하는 함수
	 * @param link Link값
	 * @return 생성된 데이타
	 */
	public byte[] CreateLinkFrame(short link){
		byte frame[] = new byte[LINK_SIZE];
		int count = 0;
		frame[count++] = LINK_FS;
		frame[count++] = (byte)(link >> 8);
		frame[count++] = (byte)link;
		frame[count++] = LINK_FE;
		return frame;
	}
	/**
	 * 해더 프레임 생성하는 함수
	 * @param c 차수
	 * @param HID HID
	 * @param p 사용자 정보 유형
	 * @param user 사용자 판별 정보
	 * @param MN 데이타 프레임 갯수
	 * @param NBS 데이타 프레임 크기
	 * @param recvPacket
	 * @return 생성된 데이타
	 */
	public byte[] CreateHeaderFrame(byte c, int HID, char p, String user , short MN , short NBS  ){
		byte header[] = new byte[HEADER_SIZE];
		try {
			byte car[] = null;
			car = CalendarFrame();
			
			int count = 0;
			for( int i = 0 ; i < FS.length ; i++ ){
				header[count++]= FS[i];
			}
			for( int i = 0 ; i < car.length ; i++ ){
				header[count++]= car[i];
			}
			
			header[count++] = c;
			
			header[count++] = (byte)(HID>>24);
			header[count++] = (byte)(HID>>16);
			header[count++] = (byte)(HID>>8);
			header[count++] = (byte)(HID);
			
			header[count++] = (byte)p;
			
			byte userInfo[] = user.getBytes();
			for( int i = 0 ; i < 13 ; i++ ){
				if( userInfo.length > i)
					header[count++] = userInfo[i];
				else 
					header[count++] = 0x00;
			}
			
			header[count++] = (byte)(MN>>8);
			header[count++] = (byte)(MN);
			
			header[count++] = (byte)(NBS>>8);
			header[count++] = (byte)(NBS);
			
			short crc = CRC16.updateH_crc((short)0, header, (short)(header.length-4));
			
			header[count++] = (byte)(crc>>8);
			header[count++] = (byte)(crc);
			
			
			for( int i = 0 ; i < FE.length ; i++ ){
				header[count++]= FE[i];
			}  
		}catch(Exception e) {
			
			return null;
		}
		return header;
	}
	
	/**
	 * 데이타 프레임을 생성하는 함수
	 * @param number 데이타 프레임 Index 
	 * @param data 데이타 부분 버퍼
	 * @return 생성된 데이타
	 */
	public byte[] CreateDataFrame(short number, byte[] data){
		byte frame[] = null;
		int count = 0;
		frame = new byte[data.length + 10];
		
		for( int i = 0 ; i < FS.length ; i++ ){
			frame[count++]= FS[i];
		}
		frame[count++] = (byte)(number>>8);
		frame[count++] = (byte)(number);
		
		frame[count++] = (byte)(data.length>>8);
		frame[count++] = (byte)(data.length);
		
		for( int i = 0 ; i < data.length ; i++ ){
			frame[count++] = data[i];
		}
		
		short crc = CRC16.updateH_crc((short)0, frame, (short)(frame.length-4));
		
		frame[count++] = (byte)(crc>>8);
		frame[count++] = (byte)(crc);
		
		for( int i = 0 ; i < FE.length ; i++ ){
			frame[count++]= FE[i];
		}
		return frame;
	}
	
	/**
	 * 현재 시간 정보
	 * @return
	 */
	private byte[] CalendarFrame(){
		Calendar car = Calendar.getInstance();
		byte time[] = {
					(byte)(car.get(Calendar.YEAR)/100),
					(byte)(car.get(Calendar.YEAR)%100),
					(byte)(car.get(Calendar.MONTH)+1),
					(byte)(car.get(Calendar.DAY_OF_MONTH)),
					(byte)(car.get(Calendar.HOUR_OF_DAY)),
					(byte)(car.get(Calendar.MINUTE)),
					(byte)(car.get(Calendar.SECOND))
				};
		return time; 
	} 
	
	 
}
