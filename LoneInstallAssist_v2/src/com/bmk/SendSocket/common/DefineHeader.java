package com.bmk.SendSocket.common;


/**
 * 해더 프레임을 사용하기 쉽게 정리해주는 클래스
 * @author BMK-GHST
 *
 */
public class DefineHeader {
	
	public byte[] buf = null;
	public short FS;
	public byte[] TIME = new byte[7];
	public byte C;
	public int HID;
	public byte T;
	public StringBuffer USERINFO = new StringBuffer();	
	public byte[] bUSERINFO = new byte[13];
	
	public short MN;
	public short NBS;
	public short CRC;	
	public short FE;
	
	public boolean is6262 = false; 
	/** 
	 * Byte 버퍼의 있는 데이타를 변수에 대입해주는 함수
	 */
	public void resetBuf(){
		if( buf == null )return;
		int count = 0;
		buf[count++] = (byte)(FS>>8);
		buf[count++] = (byte)(FS);
		
		for( int i = 0 ; i < TIME.length ; i++ ){
			buf[count++] = TIME[i];
		}
		buf[count++] = C;
		
		buf[count++] = (byte)(HID>>24);
		buf[count++] = (byte)(HID>>16);
		buf[count++] = (byte)(HID>>8);
		buf[count++] = (byte)(HID);
		buf[count++] = T;
		for( int i = 0 ; i < bUSERINFO.length ; i++ ){
			if( USERINFO.length()-1 < i){
				bUSERINFO[i] = (byte)0x00;
				buf[count++] = (byte)0x00;
				continue;
			}
			bUSERINFO[i] = (byte)USERINFO.charAt(i);
			buf[count++] = (byte)USERINFO.charAt(i);
			
		}
		
		buf[count++] = (byte)(MN>>8);
		buf[count++] = (byte)(MN);
		
		buf[count++] = (byte)(NBS>>8);
		buf[count++] = (byte)(NBS);
		
		CRC = CRC16.updateH_crc((short)0, buf, (short)(buf.length-4));
		buf[count++] = (byte)(CRC>>8);
		buf[count++] = (byte)(CRC);
		
		buf[count++] = (byte)(FE>>8);
		buf[count++] = (byte)(FE);
	}
	
	/** 
	 * 새로운 버퍼를 적용하는 함수
	 * @param buf 적용할 Buf
	 * @return 처리 결과
	 */
	public int setHeader ( byte[] buf ){
		if( buf.length != DefineList.HEADER_SIZE){
			return 1;
		}
		this.buf = new byte[buf.length];
		for( int i = 0 ; i < this.buf.length ; i++ ){
			this.buf[i] = buf[i];
		}
		int count = 0;
		FS = (short)((buf[count++]<<8) | (buf[count++]));
		for( int i = 0 ; i < TIME.length ; i++ ){
			TIME[i] = buf[count++];
		}
		C = buf[count++];
		HID = ((buf[count++]<<24)&0xFF000000)|((buf[count++]<<16)&0x00FF0000) | ((buf[count++]<<8)&0x0000FF00) | (buf[count++]&0x000000FF);
		T = buf[count++];
		
		for( int i = 0 ; i < bUSERINFO.length ; i++ ){
			bUSERINFO[i] = buf[count++];
			if( bUSERINFO[i] == 0x00 )continue;
			USERINFO.append((char)bUSERINFO[i]);
		}
		MN = (short)((buf[count++]<<8) | (buf[count++]));
		NBS = (short)((buf[count++]<<8) | (buf[count++]));
		CRC = (short)((buf[count++]<<8) | (buf[count++]));
		FE = (short)((buf[count++]<<8) | (buf[count++]));
		return 0;
	}	
}
