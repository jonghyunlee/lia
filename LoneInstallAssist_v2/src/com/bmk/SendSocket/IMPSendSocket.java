package com.bmk.SendSocket;

import java.io.IOException;
import java.util.LinkedList;

public interface IMPSendSocket {
	public static enum STATE {
		SERVER_OK,
		ERROR_SENDDATA_FAILE,
		ERROR_RECVDATA_CRC,
		ERROR_RECVDATA_FS,
		ERROR_RECVDATA_FE,
		ERROR_RECVDATA_NULL
	}; 
	/**
	 * Socket Write
	 * @param writeBuf 전송할 버퍼
	 * @return 전송 결과
	 */
	public STATE SocketWrite( byte[] writeBuf)throws IOException;
	/**
	 * Data Frame을 받기 위한 함수
	 * @param maxCount 읽어 들일 Data Frame 의 개수
	 * @param frameSize 읽어 들일 Data Frame 의 Size
	 * @return 입력 받은 결과 , 실패 시 null
	 */
	public LinkedList<byte[]> SocketReadData( short maxCount , short frameSize)throws IOException;
	
	
	/**
	 * Link 메시지를 받기 위한 함수
	 * @param readSize Read 할 Data Size
	 * @return Read 한 byte Array
	 * @throws IOException 
	 */
	public byte[] SocketReadLink( int readSize ) throws IOException;
	
	/**
	 * 소켓에서 Data를 읽기 위한 함수
	 * @param readSize Read 할 Data Size
	 * @return
	 */
	public byte[] SocketRead(  int readSize)throws IOException;
	
	
	/**
	 * Data Frame의 CRC 체크를 위한 함수
	 * @param buf 체크할 버퍼
	 * @param reCount RE-Try 카운트
	 * @return 체크 결과
	 */
	public STATE CheckCRC( byte[] buf, int reCount);
	
	
	/**
	 * Frame을 체크 하기 위한 함수 , Frame start/end 체크
	 * @param buf 체크할 버퍼
	 * @param reCount RE-Try 카운
	 * @return 체크 결과
	 */
	public STATE CheckFrame( byte[] buf , int reCount);
}
